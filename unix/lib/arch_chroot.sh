#!/bin/sh
pacman -Syu --needed --noconfirm --quiet grub grub-btrfs efibootmgr os-prober freetype2 dhcpcd iwd networkmanager reflector
ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
echo "KEYMAP=fr" > /etc/vconsole.conf
mkinitcpio -P
grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB --recheck
grub-mkconfig -o /boot/grub/grub.cfg