function Install-PSBasicTools
{
	Install-PackageProvider -Name "NuGet" -Force
	Install-Module -Name PowerShellGet -Force -Scope AllUsers
	Get-PackageSource -Name MyNuGet -ErrorAction SilentlyContinue ; if (-not $?) { Register-PackageSource -Name MyNuGet -Location https://www.nuget.org/api/v2 -ProviderName NuGet -Trusted }
	Install-Module -Name NuGet -Force -Scope AllUsers
	Install-Module -Name PSReadLine -Force -Scope AllUsers
	Install-Module -Name CompletionPredictor -Force -Scope AllUsers
}

function Install-WingetScript
{
	New-Item -Path $env:SystemDrive\ -Name Temp -ItemType Directory -ErrorAction SilentlyContinue

	# Installing Xaml, a required dependency for Winget
	Install-Package -Name Microsoft.UI.Xaml
	$XamlPath = Get-Package -Name Microsoft.UI.Xaml | Select-Object -ExpandProperty Source
	$XamlRelease = (Join-Path -Path (Split-Path -Path $XamlPath) -ChildPath "tools\AppX\x64\Release")
	$XamlAppxName = Get-ChildItem $XamlRelease | Select-Object -ExpandProperty Name
	$XamlAppxPath = (Join-Path -Path $XamlRelease -ChildPath $XamlAppxName)
	Add-AppxPackage -Path $XamlAppxPath

	# Installing VCLibs, a required dependency for Winget
	# $currentVCLibsversion = (Get-AppxPackage -Name Microsoft.VCLibs.140.00.UWPDesktop | Where-Object { $_.Architecture -eq "X64" } | Select-Object -Expand Version)
	$vclibs_url = "https://aka.ms/Microsoft.VCLibs.x64.14.00.Desktop.appx"
	$vclibs_appx = "$env:SystemDrive\Temp\vclibs.appx"
	$WebClient = (New-Object System.Net.WebClient)
	$WebClient.DownloadFile($vclibs_url, $vclibs_appx)
	Add-AppxPackage -Path $vclibs_appx
	Remove-Item $vclibs_appx

	# Installing Winget
	if ((Get-ComputerInfo -Property WindowsInstallationType).WindowsInstallationType -eq "Client")
	# If running a workstation
	{
		try
		{
			# Using WingetTools script for Windows 10
			Install-Module -Name WingetTools -Force
			Install-Winget
			Write-Host "Successfully installed WinGet using the WingetTools script." -ForegroundColor "Green"
		}
		catch
		{
			Throw (-join "Failed installing WinGet using the WingetTools script. Exiting.", $PSItem.Exception.Message)
		}
	}
	elseif ((Get-CimInstance -ClassName Win32_OperatingSystem).Version.split(".")[2] -ge 20000)
	# Running Windows Server 2022 or higher
	{
		try
		{
			# License is required on Windows Server 2022
			$WinGetXML = "$env:SystemDrive\Temp\winget_License1.xml"
			$WinGetMSIX = "$env:SystemDrive\Temp\winget.msixbundle"
			$WebClient.DownloadFile(((Invoke-RestMethod -Method GET -Uri "https://api.github.com/repos/microsoft/winget-cli/releases")[0].assets | Where-Object name -like "*_License1.xml" ).browser_download_url, $WinGetXML)
			$WebClient.DownloadFile(((Invoke-RestMethod -Method GET -Uri "https://api.github.com/repos/microsoft/winget-cli/releases")[0].assets | Where-Object name -like "*.msixbundle" ).browser_download_url, $WinGetMSIX)
			Add-AppxProvisionedPackage -Online -PackagePath $WinGetMSIX -LicensePath $WinGetXML
			Remove-Item $WinGetMSIX, $WinGetXML
			Write-Host "Successfully installed WinGet." -ForegroundColor "Green"
		}
		catch
		{
			Throw (-join "Failed installing WinGet. Exiting.", $PSItem.Exception.Message)
		}
	}
	else
	{
		Write-Host "Cannot install WinGet on this machine." -ForegroundColor "Yellow"
	}
}

function Set-WingetConfig([switch]$AutoInstall=$false)
{
	if ($AutoInstall)
	{
		$InstallFilesDir = "$env:SystemDrive\Temp\install-files\windows"
	}
	else
	{
		$InstallFilesDir = $PSScriptRoot
	}
	Write-Host "Copying WinGet configuration script." -ForegroundColor "Cyan"
	New-Item -ItemType Directory "$env:LOCALAPPDATA\Packages\Microsoft.DesktopAppInstaller_8wekyb3d8bbwe\LocalState" -ErrorAction SilentlyContinue
	New-Item -ItemType Directory "$env:SystemDrive\Users\Default\AppData\Local\Packages\Microsoft.DesktopAppInstaller_8wekyb3d8bbwe\LocalState" -ErrorAction SilentlyContinue
	if ($Lang)
	{
		(Get-Content "$InstallFilesDir\winget_settings.json").replace("`"locale`": []", "`"locale`": [ `"$Lang`" ]") | Set-Content "$env:LOCALAPPDATA\Packages\Microsoft.DesktopAppInstaller_8wekyb3d8bbwe\LocalState\settings.json"
		Copy-Item -Path "$env:LOCALAPPDATA\Packages\Microsoft.DesktopAppInstaller_8wekyb3d8bbwe\LocalState\settings.json" -Destination "$env:SystemDrive\Users\Default\AppData\Local\Packages\Microsoft.DesktopAppInstaller_8wekyb3d8bbwe\LocalState\settings.json"

	}
	else
	{
		Copy-Item -Path "$InstallFilesDir\winget_settings.json" -Destination "$env:LOCALAPPDATA\Packages\Microsoft.DesktopAppInstaller_8wekyb3d8bbwe\LocalState\settings.json"
		Copy-Item -Path "$InstallFilesDir\winget_settings.json" -Destination "$env:SystemDrive\Users\Default\AppData\Local\Packages\Microsoft.DesktopAppInstaller_8wekyb3d8bbwe\LocalState\settings.json"
	}
}


function Invoke-AutoInstall
{
	Install-PackageProvider -Name "NuGet" -Force
	Set-PSRepository -Name "PSGallery" -InstallationPolicy Trusted
	Install-Script winget-install -Force
	. "$env:ProgramFiles\WindowsPowerShell\Scripts\winget-install.ps1" -Force
	Add-AppxPackage -Path "https://cdn.winget.microsoft.com/cache/source.msix"

	while (-Not(Get-Command winget -ErrorAction SilentlyContinue))
	{
		Start-Sleep 5
	}
	winget install --exact --id Git.Git --accept-source-agreements --accept-package-agreements

	$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine"),[System.Environment]::GetEnvironmentVariable("Path","User") -join [IO.Path]::PathSeparator

	$null = New-Item -Path $env:SystemDrive\ -Name Temp -ItemType Directory -ErrorAction SilentlyContinue
	git clone https://gitlab.com/Keysuke/install-files "$env:SystemDrive\Temp\install-files"
	git clone https://gitlab.com/Keysuke/dotfiles "$env:SystemDrive\Temp\install-files\dotfiles"
	Set-WingetConfig -AutoInstall

	if ((Get-ItemPropertyValue -Path "HKLM:\SYSTEM\CurrentControlSet\Services\WinDefend" -Name "Start" -ErrorAction SilentlyContinue) -eq 4)
	{
		Write-Host "Defender is permanently disabled. Installing infosec tools." -ForegroundColor "Magenta"
		if (Test-Path "$env:SystemDrive\.remoteaccess")
		{
			. "$env:SystemDrive\Temp\install-files\windows\windows_install.ps1" -AutoInstall -InfoSec -RemoteAccess
		}
		else
		{
			. "$env:SystemDrive\Temp\install-files\windows\windows_install.ps1" -AutoInstall -InfoSec
		}
	}
	else
	{
		if (Test-Path "$env:SystemDrive\.remoteaccess")
		{
			. "$env:SystemDrive\Temp\install-files\windows\windows_install.ps1" -AutoInstall -RemoteAccess
		}
		else
		{
			. "$env:SystemDrive\Temp\install-files\windows\windows_install.ps1" -AutoInstall
		}
	}
}

function Install-Programs
{
	Write-Host "Installing programs." -ForegroundColor "Cyan"
	if (Get-Command winget -ErrorAction SilentlyContinue)
	{
		winget import -i "$PSScriptRoot\pkglist\winget_minimal.json" --accept-source-agreements --accept-package-agreements
		if(-Not ($Minimal))
		{
			winget import -i "$PSScriptRoot\pkglist\winget.json" --accept-source-agreements --accept-package-agreements
			if(-Not ($IsContainer))
			{
				winget import -i "$PSScriptRoot\pkglist\winget_gui.json" --accept-source-agreements --accept-package-agreements
			}
		}

		if($Custom)
		{
			winget import -i "$PSScriptRoot\pkglist\winget_custom.json" --accept-source-agreements --accept-package-agreements
		}
	}
	elseif (Get-Command choco -ErrorAction SilentlyContinue)
	{
		Write-Host "WinGet is not installed. Proceeding with chocolatey." -ForegroundColor "Yellow"
		choco install -y $PSScriptRoot\pkglist\chocolatey_minimal.config
		if(-Not($Minimal))
		{
			choco install -y $PSScriptRoot\pkglist\chocolatey.config
			if(-Not($IsContainer))
			{
				choco install -y $PSScriptRoot\pkglist\chocolatey_gui.config
			}
		}
		if($Custom)
		{
			choco install -y $PSScriptRoot\pkglist\chocolatey_custom.config
		}
	}
	else
	{
		Write-Host "WinGet and Chocolatey are not installed." -ForegroundColor "Red"
	}
	Install-Module -Name Terminal-Icons -Force
	Install-Module -Name PSScriptTools -Force
	if (-Not($AutoInstall) -And -(-Not($IsContainer)))
	{
		if (Get-Command winget -ErrorAction SilentlyContinue)
		{
			winget install Klocman.BulkCrapUninstaller --accept-source-agreements --accept-package-agreements
		}
		else
		{
			choco install -y bulk-crap-uninstaller
		}
		if (Test-Path "$env:ProgramFiles\BCUninstaller\win-x64\BCU-console.exe")
		{
			Write-Host "Removing bloatware." -ForegroundColor "Cyan"
			. "$env:ProgramFiles\BCUninstaller\win-x64\BCU-console.exe" uninstall "$PSScriptRoot\windows11.bcul" /U /Q
			if (Get-Command winget -ErrorAction SilentlyContinue)
			{
				winget uninstall Klocman.BulkCrapUninstaller --accept-source-agreements
			}
			else
			{
				choco uninstall -y bulk-crap-uninstaller
			}
		}
	}
}

function Disable-WindowsUpdate
{
	Stop-Service -Name wuauserv
	Set-Service -Name wuauserv -StartupType Disabled
	$WindowsUpdateKey = 'HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate'
	$AutoUpdateKey = "HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU"
	if((Test-Path -Path $WindowsUpdateKey))
	{
		Remove-Item -Path $WindowsUpdateKey -Recurse
	}
	New-Item -path $WindowsUpdateKey -Force
	New-Item -path $AutoUpdateKey -Force
	New-ItemProperty -Path $WindowsUpdateKey -Name "NoAutoUpdate" -Value 1 -propertyType "DWord"
	New-ItemProperty -Path $WindowsUpdateKey -Name "DisableWindowsUpdateAccess" -Value 1 -propertyType "DWord"
	New-ItemProperty -Path $WindowsUpdateKey -Name "SetDisableUXWUAccess" -Value 1 -propertyType "DWord"
	New-ItemProperty -Path $WindowsUpdateKey -Name "DoNotConnectToWindowsUpdateInternetLocations" -Value 1 -propertyType "DWord"
	New-ItemProperty -Path $WindowsUpdateKey -Name "DisableOSUpgrade" -Value 1 -propertyType "DWord"
}


function Set-WindowsConfig
{
	# Disabling SuperFetch
	Stop-Service -Force -Name "SysMain"
	Set-Service -Name "SysMain" -StartupType Disabled
	# Automatic timezone selection
	if (Test-Path HKLM:\SYSTEM\CurrentControlSet\Services\tzautoupdate)
	{
		Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\tzautoupdate" -Name Start -Value 3
	}

	# Locales
	if ($Lang)
	{
		Write-Host "Setting language to $Lang." -ForegroundColor "Cyan"
		if (-Not($IsContainer))
		{
			if ((Get-ComputerInfo -Property WindowsInstallationType).WindowsInstallationType -eq "Client")
			{
				Install-Language -Language $Lang
				#Set-SystemPreferredUILanguage -Language $Lang
			}
			Set-WinUserLanguageList (New-WinUserLanguageList $Lang) -Force
			Set-WinUILanguageOverride -Language $Lang
		}
		Set-WinSystemLocale -SystemLocale $Lang
		Set-Culture -CultureInfo $Lang
	}
	if ($TimeZone)
	{
		Set-TimeZone -Id $TimeZone
	}
	if ($GeoID)
	{
		Set-WinHomeLocation $GeoId
	}

	if (-Not($Minimal) -and (-Not($IsContainer)))
	{
		Start-Service -Name wuauserv
		# Windows optional features
		Enable-WindowsOptionalFeature -Online -FeatureName "NetFx3" -All -NoRestart
		$IE11_feature = Get-WindowsOptionalFeature -Online -FeatureName Internet-Explorer-Optional-amd64
		if ($IE11_feature.State -eq "Enabled")
		{
			Disable-WindowsOptionalFeature -Online -FeatureName Internet-Explorer-Optional-amd64 -NoRestart
		}
		Stop-Service -Name wuauserv
	}

	. $PSScriptRoot\..\dotfiles\install.ps1

	Install-Module PSWindowsUpdate -Force
}

function Invoke-InstallUpdates
{
	Start-Service -Name wuauserv
	Write-Host "Installing Windows updates. This will take a while..." -ForegroundColor "Cyan"
	Install-WindowsUpdate -AcceptAll -MicrosoftUpdate -Install -IgnoreReboot
	Write-Host "Finished installing Windows updates." -ForegroundColor "Cyan"
	if (-Not($Infosec))
	{
		Update-Help -Force -ErrorAction SilentlyContinue -ErrorVariable UpdateErrors
	}
}

function Enable-PasswordPolicy
{
	secedit /export /cfg c:\secpol.cfg
	(Get-Content C:\secpol.cfg) -Replace "PasswordComplexity = 0","PasswordComplexity = 1" | Out-File C:\secpol.cfg
	secedit /configure /db c:\windows\security\local.sdb /cfg c:\secpol.cfg /areas SECURITYPOLICY
	Remove-Item C:\secpol.cfg -Force
}

function Set-WindowsHardening
{
	if (((Get-WindowsEdition -Online).Edition -ne "Core") -And -Not($IsVM))
	{
		Enable-WindowsOptionalFeature -FeatureName "Containers-DisposableClientVM" -All -Online -NoRestart
	}

	New-Item "HKLM:\SOFTWARE\Policies\Microsoft\Windows NT" -Name DNSClient -Force
	New-ItemProperty "HKLM:\SOFTWARE\Policies\Microsoft\Windows NT\DNSClient" -Name EnableMultiCast -Value 0 -PropertyType DWORD -Force
	$NetBTKey = "HKLM:\SYSTEM\CurrentControlSet\services\NetBT\Parameters\Interfaces"
	Get-ChildItem $NetBTKey | ForEach-Object { Set-ItemProperty -Path "$NetBTKey\$($_.pschildname)" -Name NetbiosOptions -Value 2 -Verbose}
	Set-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Services\Dnscache\Parameters\" -Name EnableMDNS -Value 0 -Type DWord

	New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name FilterAdministratorToken -Type DWord -Value 1 -Force

	Disable-NetAdapterBinding -Name "*" -ComponentID ms_tcpip6
	Set-ItemProperty -Path "HKLM:SYSTEM\CurrentControlSet\Services\Tcpip6\Parameters" -Name DisabledComponents -Value 255 -Verbose
	netsh -f "$PSScriptRoot\block_efsr.txt"
	$LsaKeyPath =  "HKLM:\SYSTEM\CurrentControlSet\Control\Lsa"
	if((Test-Path -LiteralPath $LsaKeyPath) -ne $true)
	{
		New-Item $LsaKeyPath -force -ErrorAction SilentlyContinue
	}
	# Enable LSASS protection
	New-ItemProperty -LiteralPath $LsaKeyPath -Name 'RunAsPPL' -Value 1 -PropertyType DWord -Force -ErrorAction SilentlyContinue;

	New-ItemProperty -LiteralPath "HKLM:\SYSTEM\CurrentControlSet\Control\Lsa" -Name 'LmCompatibilityLevel' -Value 5 -PropertyType DWord -Force -ErrorAction SilentlyContinue;
	Set-SmbServerConfiguration -Confirm:$false -EncryptData $True
	Set-SmbServerConfiguration -Confirm:$false -EncryptionCiphers "AES_128_GCM , AES_256_GCM"
	Set-SmbServerConfiguration -Confirm:$false -RequireSecuritySignature $True
	Set-SmbServerConfiguration -Confirm:$false -RejectUnencryptedAccess $True
	if ((Get-CimInstance -ClassName Win32_OperatingSystem).Version.split(".")[2] -ge 26100)
	{
		#Set-SmbClientConfiguration -Confirm:$false -BlockNTLM $true
		Set-SmbClientConfiguration -Confirm:$false -RequireEncryption $true
		Set-SmbClientConfiguration -Confirm:$false -Smb2DialectMax SMB311 -Smb2DialectMin SMB311
		Set-SmbServerConfiguration -Confirm:$false -Smb2DialectMax SMB311 -Smb2DialectMin SMB311
	}
	else
	{
		Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\LanmanWorkstation\Parameters" -Name "MinSMB2Dialect" -Value 0x000000311 -Force
		Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\LanmanWorkstation\Parameters" -Name "MaxSMB2Dialect" -Value 0x000000311 -Force
	}
}

function Set-HardwareHardening
{
	$LsaKeyPath =  "HKLM:\SYSTEM\CurrentControlSet\Control\Lsa"
	if((Test-Path -LiteralPath $LsaKeyPath) -ne $true)
	{
		New-Item $LsaKeyPath -force -ErrorAction SilentlyContinue
	}
	# Enable Credential Guard - 1 with UEFI lock, 2 without lock, 0 for Disabled
	New-ItemProperty -Path $LsaKeyPath -Name LsaCfgFlags -PropertyType DWORD -Value 2 -Force

	$DeviceGuardKeyPath = "HKLM:\SYSTEM\CurrentControlSet\Control\DeviceGuard"
	If (-not(Test-Path -Path $DeviceGuardKeyPath))
	{
		New-Item -Path $DeviceGuardKeyPath -ItemType Directory -Force
	}
	# Enable Virtualization-based Security
	New-ItemProperty -Path $DeviceGuardKeyPath -Name EnableVirtualizationBasedSecurity -PropertyType DWORD -Value 1 -Force
	# Enable secure boot features - 1 for Secure Boot only, 3 for Secure Boot and DMA Protection
	New-ItemProperty -Path $DeviceGuardKeyPath -Name RequirePlatformSecurityFeatures -PropertyType DWORD -Value 1 -Force

	$HypervisorEnforcedCodeIntegrityKeyPath = "HKLM:\SYSTEM\CurrentControlSet\Control\DeviceGuard\Scenarios\HypervisorEnforcedCodeIntegrity"
	If (-not(Test-Path -Path $HypervisorEnforcedCodeIntegrityKeyPath))
	{
		New-Item -Path $HypervisorEnforcedCodeIntegrityKeyPath -ItemType Directory -Force
	}
	# Enable memory integrity
	New-ItemProperty -Path $HypervisorEnforcedCodeIntegrityKeyPath -Name "Enabled" -PropertyType DWord -Value 1 -Force
	New-ItemProperty -Path $HypervisorEnforcedCodeIntegrityKeyPath -Name "WasEnabledBy" -PropertyType DWord -Value 2 -Force

	$SystemGuardKeyPath = "HKLM:\SYSTEM\CurrentControlSet\Control\DeviceGuard\Scenarios\SystemGuard"
	If (-not(Test-Path -Path $SystemGuardKeyPath))
	{
		New-Item -Path $SystemGuardKeyPath -ItemType Directory -Force
	}
	# Enable System Guard Secure Launch
	New-ItemProperty -Path $SystemGuardKeyPath -Name "Enabled" -PropertyType DWord -Value 1 -Force
}

function Set-WindowsConfigGUI
{
	if ( -Not($NoUpdate))
	{
		# Update Windows Store apps
		Get-CimInstance -Namespace "Root\cimv2\mdm\dmmap" -ClassName "MDM_EnterpriseModernAppManagement_AppManagement01" | Invoke-CimMethod -MethodName UpdateScanMethod
	}

	# Disable fast boot
	Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager\Power" -Name HiberbootEnabled -Value 0
	# Enable hibernate
	Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Control\Power" -Name HibernateEnabled -Value 1
	# Enable numlock on boot
	Set-ItemProperty -Path "Registry::HKEY_USERS\.DEFAULT\Control Panel\Keyboard" -Name InitialKeyboardIndicators -Value 2
	if ($KeyboardLayout)
	{
		# Set keyboard layout
		Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Control\Keyboard Layouts\00000804" -Name "Layout File" -Value $KeyboardLayout
	}
	# Do not slow down search by including all public folders
	Set-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name Start_SearchFiles -Value 1
	# Hide Task View button
	Set-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name ShowTaskViewButton -Value 0
	# Hide the Meet Now icon
	Set-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" -Name HideSCAMeetNow -Value 1

	New-Item -Path "HKLM:\Software\Policies\Microsoft\Edge" -Force
	Set-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Edge" -Name HideFirstRunExperience -Value 1
	# Prevent creation of Edge shortcut on desktop
	Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" -Name DisableEdgeDesktopShortcutCreation -Value 1 -Force
	New-Item -Path "HKLM:\Software\Policies\Microsoft\Windows\Explorer" -Force
	Set-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\Explorer" -Name DisableSearchBoxSuggestions -Value 1 -Force
	Set-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\Explorer" -Name HideRecommendedSection -Value 1 -Force
	New-Item -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\" -Name "Windows Search" -Force
	New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search" -Name AllowCortana -PropertyType DWord -Value 0 -Force
	New-Item -Path "HKLM:\SOFTWARE\Policies\Microsoft" -Name "EdgeUpdate" -Force
	New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft" -Name CreateDesktopShortcutDefault -PropertyType DWord -Value 0 -Force

	if ($IsVM)
	{
		New-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\Explorer\VisualEffects\SelectionFade" -Name DefaultValue -Value 0 -PropertyType DWord -Force
		New-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\Explorer\VisualEffects\ComboBoxAnimation" -Name DefaultValue -Value 0 -PropertyType DWord -Force
		New-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\Explorer\VisualEffects\ListBoxSmoothScrolling" -Name DefaultValue -Value 0 -PropertyType DWord -Force
		New-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\Explorer\VisualEffects\FontSmoothing" -Name DefaultValue -Value 1 -PropertyType DWord -Force
	}

	foreach ($RegPath in ("Registry::HKEY_CLASSES_ROOT\ms-gamebar","Registry::HKEY_CLASSES_ROOT\ms-gamebarservices"))
	{
		New-Item -Path $RegPath -Force -Value "URL:ms-gamebar"
		New-ItemProperty -Path $RegPath -Force -Name "URL Protocol" -Value "" -PropertyType String
		New-ItemProperty -Path $RegPath -Force -Name "NoOpenWith" -Value "" -PropertyType String
		New-Item -Path "$RegPath\shell\open\command" -Force -Value "\`"$env:SystemRoot\System32\systray.exe\`""
	}
}

function Set-UserConfig
{
	New-Item -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\GameDVR" -Force
	New-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\GameDVR" -Force -Name "AppCaptureEnabled" -Value "0" -PropertyType DWord
	New-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\GameDVR" -Force -Name "NoWinKeys" -Value "1" -PropertyType DWord
	New-Item -Path "HKCU:\System\GameConfigStore" -Force
	New-ItemProperty -Path "HKCU:\System\GameConfigStore" -Force -Name "GameDVR_Enabled" -Value "0" -PropertyType DWord
	New-ItemProperty -Path "HKCU:\System\GameConfigStore" -Force -Name "GameDVR_FSEBehaviorMode" -Value "2" -PropertyType DWord

	Set-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize" -Name AppsUseLightTheme -Value 0
	Set-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize" -Name SystemUsesLightTheme -Value 0
	New-Item -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion" -Name Search -Force
	Set-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Search" -Name SearchBoxTaskbarMode -Value 0 -Type DWord -Force
	((New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() | Where-Object {$_.Path -eq "Microsoft.WindowsStore_8wekyb3d8bbwe!App"}).Verbs() | Where-Object {$_.Name.replace('&','') -match 'Désépingler de la barre des tâches|Unpin from Taskbar'} | ForEach-Object{ $_.DoIt()}
	((New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() | Where-Object {$_.Path -eq "MSEdge"}).Verbs() | Where-Object {$_.Name.replace('&','') -match 'Désépingler de la barre des tâches|Unpin from Taskbar'} | ForEach-Object{ $_.DoIt()}

	# Combine icons when taskbar is full
	Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name TaskbarGlomLevel -Value 1
	# Do not hide file extensions
	Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name HideFileExt -Value 0
	# Hide Cortana button
	Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name ShowCortanaButton -Value 0
	# Hide Task View button
	Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name ShowTaskViewButton -Value 0
	Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name TaskbarDa -Value 0 -ErrorAction SilentlyContinue
	Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name TaskbarAl -Value 0
	Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name StartShownOnUpgrade -Value 1

	if ((Get-ComputerInfo -Property WindowsInstallationType).WindowsInstallationType -eq "Client")
	{
		if (Test-Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Feeds)
		{
			# Disable News
			Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Feeds -Name ShellFeedsTaskbarViewMode -Value 2
		}
		if (Test-Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Run\OneDrive)
		{
			# Disable autostart of OneDrive
			Remove-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Run -Name OneDrive -Force
		}
	}
	oh-my-posh font install Meslo
	$Font = "MesloLGSDZ Nerd Font Mono"
	$WTsettings = "$env:LocalAppData\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json"
	Set-ItemProperty -Path HKCU:\Console -Name FaceName -Value $Font
	if (Get-Command "wt.exe" -ErrorAction SilentlyContinue)
	{
		Write-Host "Configuring Windows Terminal" -ForegroundColor "Cyan"
		if (-Not(Test-Path $WTsettings -PathType leaf))
		{
			wt cmd /c exit
			do
			{
				Start-Sleep 5
			}
			until (Test-Path $WTsettings -PathType leaf)
		}
		if (-Not(Select-String -Pattern $Font -CaseSensitive -SimpleMatch $WTsettings -Quiet))
		{
			$TerminalJson = Get-Content $WTsettings | ConvertFrom-Json
			$TerminalFace = [ordered]@{face=$Font}
			$TerminalFont = [ordered]@{font=$TerminalFace}
			$TerminalJson.profiles.defaults | Add-Member -NotePropertyMembers $TerminalFont -TypeName Asset
			$TerminalJson.profiles.list | Where-Object { $_.guid -eq '{574e775e-4f2a-5b96-ac1e-a2962a402336}' } | Add-Member -Name commandline -Value '%PROGRAMFILES%\PowerShell\7\pwsh.exe -NoLogo' -MemberType NoteProperty -Force
			$TerminalJson.profiles.list | Where-Object { $_.guid -eq '{61c54bbd-c2c6-5271-96e7-009a87ff44bf}' } | Add-Member -Name commandline -Value '%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe -NoLogo' -MemberType NoteProperty -Force
			if ($InfoSec)
			{
				$TerminalJson.defaultProfile = "{61c54bbd-c2c6-5271-96e7-009a87ff44bf}"
			}
			else
			{
				$TerminalJson.defaultProfile = "{574e775e-4f2a-5b96-ac1e-a2962a402336}"
			}
			$TerminalJson | ConvertTo-Json -depth 32 | Set-content $WTsettings
		}
		New-Item -ItemType Directory "$env:SystemDrive\Users\Default\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState" -ErrorAction SilentlyContinue
		Copy-Item -Path $WTsettings -Destination "$env:SystemDrive\Users\Default\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json"
		New-Item -Path "HKCU:\Console\%%Startup" -Force
		New-ItemProperty -Path "HKCU:\Console\%%Startup" -Name DelegationConsole -Value "{2EACA947-7F5F-4CFA-BA87-8F7FBEEFBE69}" -PropertyType String -Force
		New-ItemProperty -Path "HKCU:\Console\%%Startup" -Name DelegationTerminal -Value "{E12CFF52-A866-4C77-9A90-F570A7AA2C6B}" -PropertyType String -Force

	} else
	{
		Write-Host "Windows Terminal could not be found, not configuring it." -ForegroundColor "Magenta"
	}
	if ($IsVM)
	{
		New-ItemProperty -Path "HKCU:\Control Panel\Desktop" -Name DragFullWindows -Value 0 -PropertyType DWord -Force
		New-ItemProperty -Path "HKCU:\Control Panel\Desktop" -Name UserPreferencesMask -Value ([byte[]](0x9e,0x12,0x03,0x80,0x10,0x00,0x00,0x00)) -PropertyType Binary -Force
		New-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\VisualEffects" -Name VisualFXSetting -Value 3 -PropertyType DWord -Force
	}
}

function Set-Firefox
{
	if (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\firefox.exe' -ErrorAction SilentlyContinue)
	{
		Write-Host "Setting Firefox as the default browser." -ForegroundColor "Cyan"
		Stop-Process -ErrorAction Ignore -Name SystemSettings
		Start-Process ms-settings:defaultapps
		$ps = Get-Process -ErrorAction Stop SystemSettings
		do
		{
			Start-Sleep -Milliseconds 100
			$ps.Refresh()
		} while ([int] $ps.MainWindowHandle)
		Start-Sleep -Milliseconds 200
		$shell = New-Object -ComObject WScript.Shell
		foreach ($i in 1..4) { $shell.SendKeys('{TAB}'); Start-Sleep -milliseconds 100 }
		$shell.SendKeys("firefox"); Start-Sleep -seconds 1
		$shell.SendKeys('{TAB}'); Start-Sleep -milliseconds 100
		$shell.SendKeys('{ENTER}'); Start-Sleep -milliseconds 100
		$shell.SendKeys('{ENTER}'); Start-Sleep -milliseconds 100
		$shell.SendKeys('%{F4}')
		New-Item -Path "HKLM:\Software\Policies\Mozilla\Firefox\Extensions\Install" -Force
		New-ItemProperty -Path "HKLM:\Software\Policies\Mozilla\Firefox\Extensions\Install" -Name 1 -Value "https://addons.mozilla.org/firefox/downloads/file/4237670/ublock_origin-latest.xpi" -PropertyType ExpandString -Force
		New-ItemProperty -Path "HKLM:\Software\Policies\Mozilla\Firefox" -Name OverrideFirstRunPage -PropertyType String -Force
	}
	else
	{
		Write-Host "Firefox is not installed." -ForegroundColor "Cyan"
	}
}



function Set-RemoteAccess
{
	if (Get-Command winget -ErrorAction SilentlyContinue)
	{
		winget install --exact --id RustDesk.RustDesk --accept-source-agreements --accept-package-agreements
	}
	elseif (Get-Command choco -ErrorAction SilentlyContinue)
	{
		choco install -y rustdesk
	}
	else
	{
		Write-Host "Could not install RustDesk. Winget and/or Chocolatey are not available." -ForegroundColor "Red"
	}
	Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
	if (-Not($InfoSec) -And (Get-Command "pwsh.exe" -ErrorAction SilentlyContinue))
	{
		$DefaultShell = Get-Command "pwsh.exe" | Select-Object -ExpandProperty Source
	}
	elseif (Get-Command "powershell.exe" -ErrorAction SilentlyContinue)
	{
		$DefaultShell = Get-Command "powershell.exe" | Select-Object -ExpandProperty Source
	}
	else
	{
		Write-Host "pwsh.exe and powershell.exe not found." -ForegroundColor "Yellow"
	}
	Write-Host "Setting default SSH shell to $DefaultShell." -ForegroundColor "Cyan"
	if (Test-Path HKLM:\SOFTWARE\OpenSSH)
	{
		$null = New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value $DefaultShell -PropertyType String -Force
	}

	Start-Service sshd
	$SSHDConfigFile = "$env:ProgramData\ssh\sshd_config"

	if (-Not(Test-Path $SSHDConfigFile -PathType leaf))
	{
		Write-Host "SSH server configuration file not found. Skipping..." -ForegroundColor "Yellow"
	}
	else
	{
		Set-Service -Name sshd -StartupType 'Automatic'
		if (!(Get-NetFirewallRule -Name "OpenSSH-Server-In-TCP" -ErrorAction SilentlyContinue | Select-Object Name, Enabled))
		{
			Write-Host "Firewall Rule 'OpenSSH-Server-In-TCP' does not exist, creating it..." -ForegroundColor "Yellow"
			New-NetFirewallRule -Name 'OpenSSH-Server-In-TCP' -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22
		}
		else
		{
			Write-Host "Firewall rule 'OpenSSH-Server-In-TCP' has been created and exists." -ForegroundColor "Cyan"
		}
		(Get-Content $SSHDConfigFile).replace("#HostKey __PROGRAMDATA__/ssh/ssh_host_ed25519_key", 'HostKey __PROGRAMDATA__/ssh/ssh_host_ed25519_key') | Set-Content $SSHDConfigFile
		(Get-Content $SSHDConfigFile).replace("#PasswordAuthentication yes", 'PasswordAuthentication no') | Set-Content $SSHDConfigFile
Add-Content $SSHDConfigFile @"
# Custom SSH hardening
Match all
ChallengeResponseAuthentication no
Ciphers chacha20-poly1305@openssh.com
KexAlgorithms curve25519-sha256@libssh.org
MACs hmac-sha2-512-etm@openssh.com
CASignatureAlgorithms ssh-ed25519
HostbasedAcceptedKeyTypes ssh-ed25519
PubkeyAcceptedKeyTypes ssh-ed25519
"@
		Copy-Item -Path "$PSScriptRoot\..\unix\files\authorized_keys" -Destination "$env:ProgramData\ssh\administrators_authorized_keys"
		$AdminUser = (Get-CimInstance -ClassName Win32_UserAccount -Filter "LocalAccount = TRUE and SID like 'S-1-5-%-500'").Name
		Enable-LocalUser -Name $AdminUser
		$WinlogonPath = "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon"
		$null = New-Item -Path "$WinlogonPath" -Name SpecialAccounts -ErrorAction SilentlyContinue
		$null = New-Item -Path "$WinlogonPath\SpecialAccounts" -Name UserList -ErrorAction SilentlyContinue
		New-ItemProperty -Path "$WinlogonPath\SpecialAccounts\UserList" -Name $AdminUser -Value 0 -Type DWord -ErrorAction SilentlyContinue
	}
}

function Set-KVM
{
	if (Get-Command winget -ErrorAction SilentlyContinue)
	{
		winget install RedHat.VirtIO WinFsp.WinFsp --accept-source-agreements --accept-package-agreements
	}
	else
	{
		Write-Host "Could not install KVM guest tools. Winget and/or Chocolatey are not available." -ForegroundColor "Red"
	}
	Set-Service -Name VirtioFsSvc -StartupType Automatic
	Start-Service VirtioFsSvc
}

function Set-Vagrant
{
	try
	{
		# Automatic login as vagrant
		Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\PasswordLess\Device" -Name "DevicePasswordLessBuildVersion" -Value 0
		Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name AutoAdminLogon -Value 1
		Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name DefaultUsername -Value vagrant -type String
		Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name DefaultPassword -Value vagrant -type String
		Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name AutoLogonCount -Value 999 -type DWord
		Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name DisableCAD -Value 1 -type DWord
		Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name DisableLockWorkstation -Value 1 -type DWord
		ipconfig
		Write-Host "Successfully set vagrant automatic login." -ForegroundColor "Green"
	}
	catch
	{
		Throw (-join "Failed setting vagrant automatic login.", $PSItem.Exception.Message)
	}
}


function Clear-InstallLogs
{
	Write-Host "Clearing install logs." -ForegroundColor "Cyan"
	Get-EventLog -LogName * | ForEach-Object { Clear-EventLog $_.Log }
	Start-Sleep -Seconds 5
	Remove-Item -Recurse -Force -Exclude "UserOnce.ps1" "$env:windir\Panther\*", "$env:windir\Panther\UnattendGC\*", "$env:windir\Setup\Scripts", "$env:windir\INF\setupapi.dev.log", "$env:Public\Desktop\*.*", (-Join (([System.Environment]::GetFolderPath([System.Environment+SpecialFolder]::Desktop)).ToString(),"\*.*")), (Get-PSReadlineOption).HistorySavePath, "$env:SystemDrive\.remoteaccess", "$env:SystemDrive\TEMP", "$env:SystemDrive\tmp" -ErrorAction SilentlyContinue
	# cipher /w:C
}