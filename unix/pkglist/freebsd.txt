zsh zsh-autosuggestions neovim git tree wget rsync curl openssl xclip
fzf exa zoxide grc chezmoi
tmux zellij mosh multitail tmate bat dust duf lsof htop btop entr hexyl nnn yank logrotate lnav
ngrep nethogs autossh tcpdump whois dhcping corkscrew openresolv elinks jq miller sl cowsay
fd-find the_silver_searcher ripgrep-all py39-asciinema py39-jc
zsh-completions git-delta sd difftastic hyperfine curlie broot cheat
dtrace-toolkit py39-sshuttle rlwrap
hping3 mtr-nox11 rinetd bind-tools doggo
zstd unzip unrar 7-zip secure_delete

numlockx gnuls most