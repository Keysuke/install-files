{ config, pkgs, lib, ... }:

{
  imports = [ ./zsh/zsh.nix ];
  home.username = "root";
  home.homeDirectory = "/root";

  home.stateVersion = "24.05"; # Please read the comment before changing.

  home.packages = [
    pkgs.hello
  ];

  home.file = {
  };

  home.sessionVariables = {
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  programs.htop.enable = true;
  programs.htop.settings = {
    color_scheme = 0;
    hide_kernel_threads = 1;
    hide_userland_threads = 1;
    highlight_base_name = 1;
    highlight_megabytes = 1;
    shadow_other_users = 1;
    tree_view = 1;
    enable_mouse = 1;
  };
}
