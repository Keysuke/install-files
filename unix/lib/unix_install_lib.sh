#!/bin/sh

set_locale()
{
	if [ "$UNAME_OS" = "FreeBSD" ]; then
		echo 'me:\
			:charset=UTF-8:\
			:lang=fr_FR.UTF-8:' > ~/.login_conf
		if ! grep -q "keymap" /etc/rc.conf ; then
			echo 'keymap="fr.acc"' >> /etc/rc.conf
		fi
	else
		if [ ! "$DISTRO" = "almalinux" ] && [ ! "$DISTRO" = "centos" ] && [ -r /usr/share/i18n/locales/fr_FR ]; then
			sed -i -E "s/^# fr_FR.UTF-8.*/fr_FR.UTF-8 UTF-8/" /etc/locale.gen
			sed -i -E "s/^#fr_FR.UTF-8.*/fr_FR.UTF-8 UTF-8/" /etc/locale.gen
			locale-gen
		fi
		if [ -w /etc/initramfs-tools/initramfs.conf ]; then
			sed -i -E '/KEYMAP=/s/n/y/' /etc/initramfs-tools/initramfs.conf
		fi
		echo "KEYMAP=fr" > /etc/vconsole.conf
		if [ "$SYSTEMD" = true ]; then
			localectl set-locale LANG=fr_FR.UTF-8
			if localectl list-keymaps 2>&1 > /dev/null; then
				localectl set-keymap --no-convert fr
			fi
		fi
		if [ "$DISTRO" = "debian" ] || [ "$DISTRO" = "kali" ] || [ "$DISTRO" = "ubuntu" ] ; then
			update-locale LANG=fr_FR.UTF-8 LANGUAGE=fr_FR.UTF-8
			dpkg-reconfigure --frontend=noninteractive locales
			sed -i -E '/XKBLAYOUT/s/us/fr/' /etc/default/keyboard
			echo 'XKBMODEL="pc105"' >> /etc/default/keyboard
			echo 'XKBLAYOUT="fr"' >> /etc/default/keyboard
		fi
	fi
}

set_services()
{
	rm -rf /etc/motd
	if [ -x "$(command -v fc-cache)" ]; then
		fc-cache -rf
	fi
	if [ -w /etc/ssh/sshd_config ]; then
		if [ "$CONTAINERENV" = true ]; then
			if [ ! -e /var/run/utmp ] ; then
				touch /var/run/utmp
				chown root:utmp /var/run/utmp
				chmod g+w /var/run/utmp
			fi
			if [ -w /etc/rsyslog.conf ] ; then
				sed -i -E '/imklog/s/^/#/' /etc/rsyslog.conf
			fi
			if [ -w /etc/systemd/system/ ] ; then
				ln -sf /dev/null /etc/systemd/system/systemd-tmpfiles-setup.service
				ln -sf /dev/null /etc/systemd/system/dbus-org.freedesktop.ModemManager1.service
				ln -sf /dev/null /etc/systemd/system/NetworkManager-wait-online.service
				if [ -w /etc/systemd/system/multi-user.target.wants ] ; then
					ln -sf /dev/null /etc/systemd/system/multi-user.target.wants/binfmt-support.service
					ln -sf /dev/null /etc/systemd/system/multi-user.target.wants/ModemManager.service
				fi
			fi
			sed -i -E '/X11UseLocalhost/s/yes/no/' /etc/ssh/sshd_config
		else
			sed -i -E '/X11UseLocalhost/s/no/yes/' /etc/ssh/sshd_config
		fi
		sed -i -E '/#X11UseLocalhost/s/^#//' /etc/ssh/sshd_config
		sed -i -E '/X11Forwarding/s/no/yes/' /etc/ssh/sshd_config
		sed -i -E '/#X11Forwarding/s/^#//' /etc/ssh/sshd_config
		sed -i -E '/ssh_host_ed25519_key/s/^#//' /etc/ssh/sshd_config
		sed -i -E '/ssh_host_ecdsa_key/s/^H/#H/' /etc/ssh/sshd_config
		sed -i -E '/ssh_host_rsa_key/s/^H/#H/' /etc/ssh/sshd_config
		sed -i -E '/PermitRootLogin yes/s/yes/prohibit-password/' /etc/ssh/sshd_config
		sed -i -E '/#PermitRootLogin/s/^#//' /etc/ssh/sshd_config
		sed -i -E '/#PasswordAuthentication/s/^#//' /etc/ssh/sshd_config
		sed -i -E '/PasswordAuthentication yes/s/yes/no/' /etc/ssh/sshd_config
		if [ "$DISTRO" = "debian" -o "$DISTRO" = "kali" ] && ! grep -q "DebianBanner" /etc/ssh/sshd_config ; then
			echo "DebianBanner no" >> /etc/ssh/sshd_config
		fi
		if ! grep -q "Custom SSH hardening" /etc/ssh/sshd_config ; then
			echo "# Custom SSH hardening" >> /etc/ssh/sshd_config
			echo "Ciphers chacha20-poly1305@openssh.com" >> /etc/ssh/sshd_config
			echo "KexAlgorithms curve25519-sha256@libssh.org" >> /etc/ssh/sshd_config
			echo "MACs hmac-sha2-512-etm@openssh.com" >> /etc/ssh/sshd_config
			echo "CASignatureAlgorithms ssh-ed25519" >> /etc/ssh/sshd_config
			echo "HostbasedAcceptedKeyTypes ssh-ed25519" >> /etc/ssh/sshd_config
			echo "PubkeyAcceptedKeyTypes ssh-ed25519" >> /etc/ssh/sshd_config
		fi
	fi
}

set_dotfiles()
{
	if [ command -v chezmoi >/dev/null 2>&1 ] || [ -x /usr/local/bin/chezmoi ]; then
		echocyan "Installing dotfiles with chezmoi for user \"$USER\"."
		mkdir -p ~/.local/share/chezmoi
		cp -r $DOTFILES_DIR/. ~/.local/share/chezmoi
		cd ~/.local/share/chezmoi
		if git remote -v | head -1 | grep git@ ; then
			git remote set-url origin $(git remote -v | head -1 | cut -f 2 | cut -d ' ' -f 1 | sed -E 's#:#/#' | sed -E 's#git@#https://#' | sed -E 's#$#.git/#')
		fi
		cd -
		if [ -x /usr/local/bin/chezmoi ]; then
			/usr/local/bin/chezmoi apply --force
		else
			chezmoi apply --force
		fi
		if [ "$CONTAINERENV" = true ] ; then
			rm -rf ~/.local/share/chezmoi
		fi
	else
		echoyellow "chezmoi not found. Installing dotfiles manually for user \"$USER\"."
		sh $DOTFILES_DIR/install.sh
	fi
}

set_access()
{
	USER_1000=$(grep "1000:1000" /etc/passwd | cut -d : -f 1)
	if [ $USER_1000 ] ; then
		echocyan "Found the user name $USER_1000 as UID 1000"
	fi
	if [ "$VAGRANTENV" = true ]; then
		echocyan 'Using "vagrant" as the default user'
		DEFAULT_USER="vagrant"
	elif [ "$CONTAINERENV" = true ] ; then
		if [ -z $USER_1000 ] ; then
			echocyan "Did not find a non-privileged user for this $DISTRO installation. Adding a non-privileged user named $DEFAULT_USER."
			useradd -m $DEFAULT_USER
		fi
		echo "$DEFAULT_USER ALL=(ALL:ALL) NOPASSWD:ALL" > /etc/sudoers.d/$DEFAULT_USER
		mkdir -p /run/sshd
		ssh-keygen -A
		if [ -r "/lib/systemd/system/ssh.service" ] ; then
			[ ! -e "/etc/systemd/system/sshd.service" ] && ln -sf /lib/systemd/system/ssh.service /etc/systemd/system/sshd.service
			[ ! -e "/etc/systemd/system/multi-user.target.wants/ssh.service" ] && mkdir -p /etc/systemd/system/multi-user.target.wants && ln -sf /lib/systemd/system/ssh.service /etc/systemd/system/multi-user.target.wants/ssh.service
		elif [ -r "/usr/lib/systemd/system/sshd.service" ] ; then
			[ ! -e "/etc/systemd/system/sshd.service" ] && ln -sf /usr/lib/systemd/system/sshd.service /etc/systemd/system/sshd.service
			[ ! -e "/etc/systemd/system/multi-user.target.wants/sshd.service" ] && mkdir -p /etc/systemd/system/multi-user.target.wants && ln -sf /usr/lib/systemd/system/sshd.service /etc/systemd/system/multi-user.target.wants/sshd.service
		fi
	elif [ "$WSL" = true ] ; then
		if [ -z $USER_1000 ] ; then
			echored "Did not find a non-privileged user for this $DISTRO installation. Please restart the WSL installation and set a non-privileged user."
			exit 1
		else
			DEFAULT_USER=$USER_1000
		fi
	else
		DEFAULT_USER=$USER_1000
	fi

	if [ ! -z $DEFAULT_USER ]; then
		if [ "$DISTRO" = "arch" ]; then
			if [ "$CUSTOM_INSTALL" = "true" ]; then
				USER_INSTALL_ARG=" -c"
			fi
			if [ "$INFOSEC_INSTALL" = "true" ]; then
				USER_INSTALL_ARG="$USER_INSTALL_ARG -i"
			fi
			runuser -l $DEFAULT_USER -c "sh $SCRIPT_DIR/unix_install.sh $USER_INSTALL_ARG"
		fi
		chsh -s $(command -v zsh) $DEFAULT_USER
	else
		echoyellow "The default user is undefined. Skipping the dotfiles configuration."
	fi

	if [ "$VAGRANTENV" = true ] || [ "$CONTAINERENV" = true ] ; then
		echocyan "Setting SSH access for users \"root\" and \"$DEFAULT_USER\""
		mkdir -p ~/.ssh
		chmod 700 ~/.ssh
		cat $SCRIPT_DIR/files/authorized_keys >> ~/.ssh/authorized_keys
		su - $DEFAULT_USER -c "mkdir -p ~/.ssh && touch ~/.ssh/authorized_keys && chmod -R go= ~/.ssh"
		cat $SCRIPT_DIR/files/authorized_keys >> $(eval echo ~$DEFAULT_USER)/.ssh/authorized_keys
	fi
}

set_console()
{
	if [ $DISTRO = "debian" ] || [ $DISTRO = "kali" ] ; then
		echo virtio_console >> /etc/initramfs-tools/modules
		update-initramfs -u
	elif [ $DISTRO = "alma" ] || [ "$DISTRO" = "centos" ] ; then
		dracut --add-drivers virtio_console --force
	elif [ $DISTRO = "arch" ] ; then
		sed -i -E '/^MODULES=(.*)$/s/)$/ virtio_console)/' /etc/mkinitcpio.conf
		mkinitcpio -P
	fi

	if [ $DISTRO = "freebsd" ]; then
		echo 'console="comconsole"' >> /boot/loader.conf
		echo 'comconsole_speed="115200"' >> /boot/loader.conf
		sed -i -E 's/vt100/vt220/g' /etc/ttys
	else
		if grep -q "GRUB_TERMINAL_INPUT=" /etc/default/grub ; then
			sed -i -E '/GRUB_TERMINAL_INPUT/s/=.*$/="console serial"/' /etc/default/grub
			sed -i -E '/#GRUB_TERMINAL_INPUT/s/^#//' /etc/default/grub
		else
			echo 'GRUB_TERMINAL_INPUT="console serial"' >> /etc/default/grub
		fi
		if grep -q "GRUB_TERMINAL_OUTPUT=" /etc/default/grub ; then
			sed -i -E '/GRUB_TERMINAL_OUTPUT/s/=.*$/="console serial"/' /etc/default/grub
			sed -i -E '/#GRUB_TERMINAL_OUTPUT/s/^#//' /etc/default/grub
		else
			echo 'GRUB_TERMINAL_OUTPUT="console serial"' >> /etc/default/grub
		fi

		if grep -q "GRUB_TIMEOUT_STYLE" /etc/default/grub ; then
			sed -i -E '/GRUB_TIMEOUT_STYLE=/s/=.*$/=menu/' /etc/default/grub
		else
			echo "GRUB_TIMEOUT_STYLE=menu" >> /etc/default/grub
		fi

		if ! grep -q "GRUB_SERIAL_COMMAND" /etc/default/grub ; then
			echo 'GRUB_SERIAL_COMMAND="serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1"' >> /etc/default/grub
		fi

		if grep -q "GRUB_CMDLINE_LINUX_DEFAULT" /etc/default/grub ; then
			sed -i -E '/GRUB_CMDLINE_LINUX_DEFAULT/s/vga=.* nomodeset//' /etc/default/grub
			sed -i -E '/GRUB_CMDLINE_LINUX_DEFAULT/s/"$/ console=tty0 console=hvc0 console=ttyS0,115200"/' /etc/default/grub
		else
			echo 'GRUB_CMDLINE_LINUX_DEFAULT="console=tty0 console=hvc0 console=ttyS0,115200"' >> /etc/default/grub
		fi
	fi
}


set_desktop()
{
	if [ ! $DISTRO = "arch" ] && [ ! $DISTRO = "kali" ]; then
		wget -nv https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf -P /usr/local/share/fonts
		wget -nv https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf -P /usr/local/share/fonts
		wget -nv https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf -P /usr/local/share/fonts
		wget -nv https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf -P /usr/local/share/fonts
	fi
	if [ -w /etc/default/grub ] && [ ! grep -q "DejaVuSansMono" /etc/default/grub ]; then
		if fc-list | grep DejaVuSansMono.ttf > /dev/null 2>&1 ; then
			DejaVuSansMono_TTF=$(fc-list | grep DejaVuSansMono.ttf | cut -d : -f 1)
			if [ $DISTRO = "alma" ] || [ $DISTRO = "centos" ] || [ $DISTRO = "kali" ] || [ $DISTRO = "arch" ] ; then
				grub-mkfont -s 22 -o /boot/grub/fonts/DejaVuSansMono.pf2 $DejaVuSansMono_TTF
				echo 'GRUB_FONT="/boot/grub/fonts/DejaVuSansMono.pf2"' >> /etc/default/grub
			else
				grub2-mkfont -s 22 -o /boot/grub2/fonts/DejaVuSansMono.pf2 $DejaVuSansMono_TTF
				echo 'GRUB_FONT="/boot/grub2/fonts/DejaVuSansMono.pf2"' >> /etc/default/grub
			fi
		fi
		echo 'GRUB_SAVEDEFAULT="true"' >> /etc/default/grub
		echo 'GRUB_DISABLE_SUBMENU=y' >> /etc/default/grub
		sed -i '/^GRUB_DEFAULT=/s/0/saved/' /etc/default/grub
	fi
	if [ "$SYSTEMD" = true ]; then
		mkdir -p /etc/systemd/system/getty@tty1.service.d
		printf "[Service]\nTTYVTDisallocate=no" > /etc/systemd/system/getty@tty1.service.d/noclear.conf
	fi

	cat << EOF >> /etc/profile.d/firefox_wayland.sh
if [ "\$XDG_SESSION_TYPE" = "wayland" ]; then
	export MOZ_ENABLE_WAYLAND=1
fi
EOF

}
