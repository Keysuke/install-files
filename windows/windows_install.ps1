#Requires -RunAsAdministrator
Param
(
	[switch]$Custom=$false,
	[switch]$NoUpdate=$false,
	[switch]$Help=$false,
	[switch]$H=$false,
	[switch]$InfoSec=$false,
	[switch]$Minimal=$false,
	[switch]$AutoInstall=$false,
	[switch]$NoSoftwareHardening=$false,
	[switch]$NoHardwareHardening=$false,
	[switch]$RemoteAccess=$false,
	[string]$Hostname,
	[string]$Lang,
	[string]$Keyboard,
	[int]$GeoID,
	[string]$TimeZone,
	[string]$DNS
)
#$ErrorActionPreference = "Stop"

if ($PSVersionTable.PSEdition -eq "Core")
{
	Write-Host "This script does not support pwsh.exe. Please use powershell.exe." -ForegroundColor "Red"
	Exit 1
}

if(($Help) -or ($H))
{
	$HelpText = @"
Usage: $PSCommandPath [options]
	-Custom               Use custom install packages
	-NoUpdate             Skip Windows updates
	-Hostname <name>      Rename the host to <name>
	-InfoSec              Set environment for information security
	-Minimal              Install a minimal set of tools
	-NoSoftwareHardening  Disable Windows hardening enhancements
	-NoHardwareHardening  Disable hardware hardening enhancements
	-RemoteAccess         Enable remote access using RustDesk and SSH
	-Help                 Display this help and exit
	-Lang                 Set the language of Windows
	-Keyboard             Specify the keyboard layout
	-GeoID                Specify the geographical location <https://learn.microsoft.com/en-us/windows/win32/intl/table-of-geographical-locations>
	-TimeZone             Specify the time zone <https://learn.microsoft.com/en-us/windows-hardware/manufacture/desktop/default-time-zones>
	-DNS                  Set the DNS

"@
	Write-Output $HelpText
	Exit 1
}

if ($PSBoundParameters)
{
	Write-Host "Running the install scripts with the following parameters: ", $PSBoundParameters -ForegroundColor "Magenta"
}

if (Test-Path "$PSScriptRoot\..\dotfiles" -PathType Container)
{
	Write-Host "Found dotfiles directory." -ForegroundColor "Cyan"
}
else
{
	Write-Host "Failed accessing dotfiles directory at $PSScriptRoot\..\dotfiles. Aborting." -ForegroundColor "Red"
	Exit 1
}


if((Get-Service -Name cexecsvc -ErrorAction SilentlyContinue))
{
	Write-Host "Running in a container." -ForegroundColor "Cyan"
	$IsContainer = $True
}
elseif (((Get-CimInstance win32_computersystem -Property Model).Model -eq "Standard PC (Q35 + ICH9, 2009)") -or ((Get-CimInstance win32_computersystem -Property Model).Model -eq "BXPC____"))
{
	Write-Host "Running in a KVM machine." -ForegroundColor "Cyan"
	$KVM = $True
	$IsVM = $True
}
elseif ((Get-CimInstance win32_computersystem -Property Model).Model -eq "VirtualBox")
{
	Write-Host "Running in a VirtualBox machine." -ForegroundColor "Cyan"
	$VirtualBox = $True
	$IsVM = $True
}
elseif ((Get-CimInstance win32_computersystem -Property Model).Model -eq "VMware Virtual Platform")
{
	Write-Host "Running in a VMware machine." -ForegroundColor "Cyan"
	$VMware = $True
	$IsVM = $True
}
elseif ((Get-CimInstance win32_computersystem -Property Model).Model -eq "Virtual Machine")
{
	Write-Host "Running in a Hyper-V machine." -ForegroundColor "Cyan"
	#$HyperV = $True
	$IsVM = $True
}
elseif (Get-WmiObject MSAcpi_ThermalZoneTemperature -Namespace "root/wmi" -ErrorAction SilentlyContinue)
{
	Write-Host (-Join ("Did not detect any virtual machine provider (", (Get-CimInstance win32_computersystem -Property Model).Model, ") and any temperature sensor. Waiting 5 seconds...")) -ForegroundColor "Magenta"
	Start-Sleep -s 5
}
if ($IsVM -eq $True)
{
	Write-Host "Running in a virtual machine. Disabling sleep options and setting hardware clock to UTC." -ForegroundColor "Cyan"
	powercfg.exe -x -monitor-timeout-ac 0
	powercfg.exe -x -monitor-timeout-dc 0
	powercfg.exe -x -disk-timeout-ac 0
	powercfg.exe -x -disk-timeout-dc 0
	powercfg.exe -x -standby-timeout-ac 0
	powercfg.exe -x -standby-timeout-dc 0
	powercfg.exe -x -hibernate-timeout-ac 0
	powercfg.exe -x -hibernate-timeout-dc 0
	Set-ItemProperty -Path HKLM:\System\CurrentControlSet\Control\TimeZoneInformation -Name "RealTimeIsUniversal" -Value 1 -type DWord
}

if(($InfoSec) -and ((Get-MpComputerStatus | Select-Object -ExpandProperty "IsTamperProtected") -eq $true))
{
	Write-Host 'Tamper protection is enabled. Please disable it before the end of the script.' -ForegroundColor "Magenta"
}


Set-ExecutionPolicy Bypass -Scope Process -Force

$null = New-Item -Path $env:SystemDrive -Name TEMP -ItemType Directory -ErrorAction SilentlyContinue

try
{
	. $PSScriptRoot\windows_install_lib.ps1
}
catch
{
	Throw (-join "Failed sourcing the $PSScriptRoot\windows_install_lib.ps1 file. Exiting.", $PSItem.Exception.Message)
}

if (Test-Path "$PSScriptRoot\custom.ps1" -PathType leaf)
{
	Write-Host "Detected a custom.ps1 script. Running it." -ForegroundColor "Cyan"
	try
	{
		. $PSScriptRoot\custom.ps1
	}
	catch
	{
		Throw (-join "Failed running the $PSScriptRoot\custom.ps1 script. Exiting", $PSItem.Exception.Message)
	}
}


if ($DNS)
{
	$FirstAdapter = (Get-NetAdapter | Select-Object -First 1)
	Set-DnsClientServerAddress -InterfaceIndex ($FirstAdapter.ifIndex) -ServerAddresses ($DNS)
	Write-Host "Setting DNS to $DNS for the following interface:" $FirstAdapter.Name -ForegroundColor "Cyan"
}

if ($IsContainer)
{
	Write-Host "Running in a container environment. Skipping Winget installation." -ForegroundColor "Cyan"
}
elseif (-Not($AutoInstall))
{
	Install-PackageProvider -Name "NuGet" -Force
	Set-PSRepository -Name "PSGallery" -InstallationPolicy Trusted
	Install-Script winget-install -Force
	. "$env:ProgramFiles\WindowsPowerShell\Scripts\winget-install.ps1" -Force
	Add-AppxPackage -Path "https://cdn.winget.microsoft.com/cache/source.msix"
	$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine"),[System.Environment]::GetEnvironmentVariable("Path","User") -join [IO.Path]::PathSeparator
	Set-WingetConfig
}

if (-Not(Get-Command choco -ErrorAction SilentlyContinue))
{
	Write-Host "Installing Chocolatey" -ForegroundColor "Cyan"
	[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
	Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
}

Install-PSBasicTools

if ($KVM)
{
	Set-KVM
}
elseif ($VirtualBox)
{
	choco install -y virtualbox-guest-additions-guest.install
}
elseif ($VMware)
{
	choco install -y vmware-tools
}

Install-Programs

$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine"),[System.Environment]::GetEnvironmentVariable("Path","User") -join [IO.Path]::PathSeparator
Set-WindowsConfig

if ($InfoSec)
{
	Write-Host "Will install infosec tools." -ForegroundColor "Cyan"
	try
	{
		. $PSScriptRoot\infosec_tools.ps1
	}
	catch
	{
		Throw (-join "Failed running the InfoSec script. Exiting.", $PSItem.Exception.Message)
	}
}

if (-Not($IsContainer))
{
	if ([System.Security.Principal.WindowsIdentity]::GetCurrent().Name.EndsWith('vagrant'))
	{
		Set-Vagrant
	}
	else
	{
		if ($RemoteAccess)
		{
			Set-RemoteAccess
		}
		Set-Firefox
	}

	if (-Not($InfoSec) -And -Not($NoSoftwareHardening))
	{
		Set-WindowsHardening
	}

	Set-WindowsConfigGUI
	Set-UserConfig

	if (-Not($IsVM) -And -Not($NoHardwareHardening))
	{
		Set-HardwareHardening
	}
}


if (-Not($NoUpdate))
{
	Invoke-InstallUpdates
}
if ($InfoSec)
{
	Disable-WindowsUpdate
}

if (-Not($IsContainer))
{
	if ($AutoInstall)
	{
		Clear-InstallLogs
	}
	cleanmgr /verylowdisk /sagerun:1 | Out-Null

	$timeout=30
	Write-Host "Finished installation. Will reboot in $timeout seconds. Press a key to prevent the automatic reboot." -ForegroundColor "Magenta"
	$stopwatch = [System.Diagnostics.Stopwatch]::StartNew()
	while (-not [Console]::KeyAvailable)
	{
		if ($stopwatch.ElapsedMilliseconds -gt $timeout*1000 )
		{
			if ($Hostname)
			{
				Rename-Computer -NewName $Hostname -Restart -Force
			}
			else
			{
				Restart-Computer -Force
			}
		}
		Start-Sleep -Seconds 1
		$countdown=$timeout - [Math]::Round(($stopwatch.ElapsedMilliseconds/1000),0)
		if ( $countdown -gt 0 )
		{
			Write-Host "$countdown seconds left." -ForegroundColor "Cyan"
		}
	}
	$stopwatch.Stop()
	Write-Host "Automatic reboot cancelled." -ForegroundColor "Magenta"

}

Exit 0
