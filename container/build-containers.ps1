#Requires -RunAsAdministrator

docker build --tag registry.gitlab.com/keysuke/install-files/windows -f Containerfile-windows ..
docker push registry.gitlab.com/keysuke/install-files/windows

docker build --tag registry.gitlab.com/keysuke/install-files/windows-infosec -f Containerfile-windows-infosec ..
docker push registry.gitlab.com/keysuke/install-files/windows-infosec