#!/bin/sh

script_name=$(echo "$0" | cut -d / -f 2)
echo "Removing files in $(CDPATH= cd -- "$(dirname -- "$0")" && pwd)"
find . \( ! -name $script_name ! -name . ! -name .. \) -exec rm -rf {} +

CHERRYTREE_URL=$(curl -s https://api.github.com/repos/giuspen/cherrytree/releases/latest | grep "browser_download_url.*" | grep "win64_setup.exe" | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)

SEVENZIP_URL=$(echo https://www.7-zip.org/$( curl -s https://www.7-zip.org/download.html | grep '\-x64.msi' | head -1 | awk 'BEGIN {FS="href="} {print $2}' | cut -d \" -f 2))

LATEST_BURP_VERSION=$(curl --silent https://portswigger.net/burp/releases --stderr - | grep "Professional / Community" | head -1 | sed -e 's/<[^>]*>//g' | awk '{$1=$1};1' | cut -d ' ' -f 4 | tr -d '\r')
BURP_COMMUNITY_URL="https://portswigger.net/burp/releases/download?product=community&type=WindowsX64&version=$LATEST_BURP_VERSION"

NMAP_URL=$(curl https://nmap.org/download#windows | grep 'setup.exe' | awk 'BEGIN {FS="href="} {print $2}' | cut -d \" -f 2)

FIREFOX_URL="https://download.mozilla.org/?product=firefox-latest&os=win64&lang=fr"

PERL_URL=$(curl -s https://api.github.com/repos/StrawberryPerl/Perl-Dist-Strawberry/releases/latest | grep "browser_download_url.*" | grep "64bit.msi" | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)

LATEST_VIRTUALBOX_VERSION=$(curl https://download.virtualbox.org/virtualbox/LATEST.TXT)
VIRTUALBOX_EXE=$(curl "https://download.virtualbox.org/virtualbox/$LATEST_VIRTUALBOX_VERSION" | grep 'Win.exe' | awk 'BEGIN {FS="href="} {print $2}' | cut -d \" -f 2)
VIRTUALBOX_URL=$(echo "https://download.virtualbox.org/virtualbox/$LATEST_VIRTUALBOX_VERSION/$VIRTUALBOX_EXE")

WIRESHARK_URL=https://1.eu.dl.wireshark.org/win64/Wireshark-latest-x64.exe

CHEATENGINE_URL=$(curl https://www.cheatengine.org/downloads.php | grep download_link | grep -v Mac | awk 'BEGIN {FS="href="} {print $2}' | cut -d \" -f 2)

set -- $CHERRYTREE_URL $SEVENZIP_URL $BURP_COMMUNITY_URL $NMAP_URL $FIREFOX_URL $PERL_URL $VIRTUALBOX_URL $WIRESHARK_URL $CHEATENGINE_URL
for url in "$@"; do wget --content-disposition -nv "$url"; done

git clone https://github.com/sullo/nikto
git clone https://github.com/sqlmapproject/sqlmap.git

GOBUSTER_WIN64_URL=$(curl -s https://api.github.com/repos/OJ/gobuster/releases/latest | grep "browser_download_url.*" | grep "Windows_x86_64" | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
wget -qO- $GOBUSTER_WIN64_URL | bsdtar -xvf- gobuster.exe
SSLSCAN_URL=$(curl -s https://api.github.com/repos/rbsec/sslscan/releases/latest | grep "browser_download_url.*" | grep ".zip" | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
wget -qO- $SSLSCAN_URL | bsdtar -xvf- sslscan.exe
NUCLEI_URL=$(curl -s https://api.github.com/repos/projectdiscovery/nuclei/releases/latest | grep "browser_download_url.*" | grep "windows_amd64.zip" | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
wget -qO- $NUCLEI_URL | bsdtar -xvf- nuclei.exe

curl --silent "https://www.python.org/ftp/python/" | sed -n 's!.*href="\([0-9]\+\.[0-9]\+\.[0-9]\+\)/".*!\1!p' | sort -rV |
while read -r version; do
	filename="python-$version-amd64.exe"
	# Versions which only have alpha, beta, or rc releases will fail here.
	# Stop when we find one with a final release.
	if \curl --fail --silent -O "https://www.python.org/ftp/python/$version/$filename"; then
		echo "Downloaded $filename"
		break
	fi
done
