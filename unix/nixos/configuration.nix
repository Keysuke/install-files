{ config, pkgs, ... }:

{
	nix.settings.experimental-features = [ "nix-command" "flakes" ];
	imports = [ ./hardware-configuration.nix ];

	boot.loader.grub.enable = true;
	boot.loader.grub.device = "/dev/vda";
	boot.loader.grub.useOSProber = true;
	boot.kernelParams = [ "console=ttyS0,115200" "console=tty0" "console=hvc0" ];
	boot.loader.grub.extraConfig = ''
	serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1
	terminal_input --append serial
	terminal_output --append serial
	'';
	networking.hostName = "nixos";
	networking.networkmanager.enable = true;

	time.timeZone = "Europe/Paris";
	i18n.defaultLocale = "en_US.UTF-8";
	i18n.extraLocaleSettings = {
		LC_ADDRESS = "fr_FR.UTF-8";
		LC_IDENTIFICATION = "fr_FR.UTF-8";
		LC_MEASUREMENT = "fr_FR.UTF-8";
		LC_MONETARY = "fr_FR.UTF-8";
		LC_NAME = "fr_FR.UTF-8";
		LC_NUMERIC = "fr_FR.UTF-8";
		LC_PAPER = "fr_FR.UTF-8";
		LC_TELEPHONE = "fr_FR.UTF-8";
		LC_TIME = "fr_FR.UTF-8";
	};

	services.xserver.xkb = {
		layout = "fr";
		variant = "";
	};

	console.keyMap = "fr";

	# Define a user account. Don't forget to set a password with ‘passwd’.
	users.users.idiot = {
		isNormalUser = true;
		description = "idiot";
		extraGroups = [ "networkmanager" "wheel" ];
		packages = with pkgs; [];
	};
	users.defaultUserShell = pkgs.zsh;
    programs.zsh.enable = true;

	users.users."root".openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGu7MjwO02ig6GymhbgjLsapm3eTOQp4hwPgIn/uNbtc" ];
	nixpkgs.config.allowUnfree = true;

	environment.systemPackages = with pkgs;
	[
		git tree wget curl curlie rsync openssl xclip gpm plocate
		fzf eza grc vivid delta
		tmux mosh tmate dust duf lsof btop entr hexyl nnn yank logrotate lnav
		bat bat-extras.prettybat bat-extras.batwatch bat-extras.batpipe bat-extras.batman bat-extras.batgrep bat-extras.batdiff
		ngrep nethogs autossh tcpdump whois dhcping corkscrew openresolv elinks jq miller sl cowsay
		zsh-autocomplete zsh-completions nix-zsh-completions zsh-autosuggestions zsh-powerlevel10k zsh-fast-syntax-highlighting zsh-history-search-multi-word
		strace sshuttle traceroute bc rlwrap
		hping mtr iproute2 dig dogdns nettools iptables
		zstd unzip unrar p7zip zip
		numlockx less acl file libxkbcommon fontconfig
		(neovim.override
		{
			viAlias = true;
			vimAlias = true;
			configure =
			{
				customRC = (builtins.readFile ./init.vim);
				packages.myPlugins = with pkgs.vimPlugins; {
				start = [ vim-nix vim-airline vim-airline-themes catppuccin-nvim rainbow nerdtree nerdcommenter nvim-surround vim-oscyank syntastic ctrlp-vim ];
				opt = [];
				};
			};
		})
	];

	services.openssh.enable = true;
	system.stateVersion = "24.05";

}
