#!/bin/sh

PARAM=$1
POOL=${PARAM:=default}
POOL_PATH=$(sudo virsh pool-dumpxml $POOL | grep path | awk '{$1=$1};1' | sed -e 's/<[^>]*>//g')

if [ -z $POOL_PATH ]; then
	echo "Could not find the storage pool path for $POOL"
	exit 1
else
	echo "Found storage pool $POOL with path $POOL_PATH"
fi

echo "Removing dead links."
sudo find -L $POOL_PATH -type l -print -delete

echo "Linking the following boxes to the $POOL pool."
for box in ~/.vagrant.d/boxes/*; do
	LINKED=false
	for version in $(ls -1 ~/.vagrant.d/boxes/$box | grep -v metadata_url); do
		for arch in $(ls -1 ~/.vagrant.d/boxes/$box/$version); do
			for provider in $(ls -1 ~/.vagrant.d/boxes/$box/$version/$arch); do
				if box_filename=$(ls -A1q ~/.vagrant.d/boxes/$box/$version/$arch/$provider | grep .img) ; then
					baseimgname=$box\_vagrant_box_image_$version\_$box_filename
					baseimg=~/.vagrant.d/boxes/$box/$version/$arch/$provider/$box_filename
					find $POOL_PATH -xtype l -exec rm {} \;
					if [ -r "$baseimg" ] ; then
						sudo ln -vsf $baseimg $POOL_PATH/$baseimgname
						LINKED=true
					fi
				else
					echo "Removing empty directory at ~/.vagrant.d/boxes/$box/$version"
					rmdir ~/.vagrant.d/boxes/$box/$version
				fi
			done
		done
		if ! $LINKED ; then
				for provider in $(ls -1 ~/.vagrant.d/boxes/$box/$version); do
				if ls -A1q ~/.vagrant.d/boxes/$box/$version/$provider | grep -q . ; then
					baseimgname=$box\_vagrant_box_image_$version\_box.img
					baseimg=~/.vagrant.d/boxes/$box/$version/$provider/box.img
					find $POOL_PATH -xtype l -exec rm {} \;
					if [ -r "$baseimg" ] ; then
						sudo ln -vsf $baseimg $POOL_PATH/$baseimgname
						LINKED=true
					fi
				else
					echo "Removing empty directory at ~/.vagrant.d/boxes/$box/$version"
					rmdir ~/.vagrant.d/boxes/$box/$version
				fi
			done
		fi
	done
done

echo -n "Refreshing the $POOL pool... "
sudo virsh pool-refresh $POOL