#!/bin/sh

disable_systemd_service()
{
	for service in "$@"; do
		if [ $(systemctl list-unit-files "$service*" | wc -l) -gt 3 ]; then
			echocyan "Disabling the $service service."
			systemctl disable $service
		fi
	done
}

debian_install()
{
	export DEBIAN_FRONTEND=noninteractive
	if [ $DISTRO = "debian" ]; then
		PKG_LIST=$(echo $PKG_LIST exa)
		cat << 'EOF' > /etc/apt/sources.list
deb http://deb.debian.org/debian stable main contrib non-free non-free-firmware
deb http://deb.debian.org/debian stable-updates main contrib non-free non-free-firmware
deb http://deb.debian.org/debian stable-backports main contrib non-free non-free-firmware
deb http://deb.debian.org/debian-security/ stable-security main contrib non-free non-free-firmware
EOF
	fi
	echocyan "Updating packages"
	apt-get -qq update
	dpkg -l grub-pc > /dev/null 2>&1 && apt-mark hold grub-pc
	apt-get -qq dist-upgrade -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confnew"
	if [ -f "/dev/input/mice" ]; then
		PKG_LIST=$(echo $PKG_LIST gpm)
	fi

	echocyan "Installing packages"
	# Trying twice in case of network error
	apt-get -yq install $PKG_LIST || apt-get -yq install $PKG_LIST

	echocyan "Finished installing apt packages."
	GITDELTA_URL=$(curl -s https://api.github.com/repos/dandavison/delta/releases/latest | grep "browser_download_url.*" | grep $PLATFORM.deb | grep -v musl | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2 | head -1)
	wget -nv $GITDELTA_URL -O /tmp/git-delta.deb
	dpkg -i /tmp/git-delta.deb
	rm -rf /tmp/git-delta.deb

	dpkg -l grub-pc > /dev/null 2>&1 && apt-mark unhold grub-pc
	apt-get -qq autoremove --purge
	apt-get clean
	if [ "$SYSTEMD" = true ]; then
		disable_systemd_service exim4 rpcbind.service rpcbind.socket avahi-daemon.socket avahi-daemon.service cups-browsed.service cups.service
		if [ -f "/dev/input/mice" ]; then
			systemctl enable gpm
		fi
	fi
	apt-get update
	update-command-not-found
}

kali_install()
{
	# if [ $VAGRANTENV = true ]; then
	# 	echocyan 'Setting repo to "kali-last-snapshot" with "main contrib non-free non-free-firmware".'
	# 	echo "deb http://http.kali.org/kali kali-last-snapshot main contrib non-free non-free-firmware" > /etc/apt/sources.list
	# fi
	if [ "$SYSTEMD" = true ] && [ $(systemd-detect-virt) = "kvm" ]; then
		echocyan "KVM detected. Removing Xfce desktop since it does not support the QEMU guest tools. Installing KDE."
		apt-get -qq autoremove --purge kali-desktop-xfce
		PKG_LIST=$(echo $PKG_LIST spice-vdagent kali-desktop-kde)
	fi
	if [ $PLATFORM = "amd64" ]; then
		echocyan "Installing Kali amd64 dependencies."
		apt-get -qq update
		apt-get -qq install wget gpg curl
		wget -q https://packages.microsoft.com/config/debian/11/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
		dpkg -i packages-microsoft-prod.deb > /dev/null
		rm -rf packages-microsoft-prod.deb
		dpkg --add-architecture i386
		PKG_LIST=$(echo $PKG_LIST veil wine32 libc6-dev-i386 libc6-dbg:i386 pspy)
	fi
	if [ "$CONTAINERENV" = false ]; then
		PKG_LIST=$(echo $PKG_LIST docker.io podman podman-compose)
	fi

	debian_install
	wget -nv https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/posh-linux-$PLATFORM -O /usr/local/bin/oh-my-posh
	chmod +x /usr/local/bin/oh-my-posh
	if [ "$SYSTEMD" = true ]; then
		systemctl enable netfilter-persistent
		disable_systemd_service smartmontools.service
	fi
	ln -sf /usr/lib/python3/dist-packages/Cryptodome /usr/lib/python3/dist-packages/Crypto
	if command -v pwsh >/dev/null 2>&1; then
		rm -rf /opt/microsoft/powershell/7/Modules/Terminal-Icons
		pwsh -c 'Install-Module -Name Terminal-Icons -Force'
		mv ~/.local/share/powershell/Modules/Terminal-Icons /opt/microsoft/powershell/7/Modules
	fi
}

arch_install()
{
	if [ "$CONTAINERENV" = false ]; then
		sed -i -E '/#Color/s/^#//' /etc/pacman.conf
		sed -i -E '/#VerbosePkgLists/s/^#//' /etc/pacman.conf
		sed -i -E "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf
	else
		if ! grep -q "ILoveCandy" /etc/pacman.conf; then
			echo $'[options]\nColor\nILoveCandy' >> /etc/pacman.conf
			echo $'[multilib]\nInclude = /etc/pacman.d/mirrorlist' >> /etc/pacman.conf
		fi
	fi
	if [ ! "$(head -1 /etc/pacman.d/mirrorlist)" = 'Server = https://mirrors.gandi.net/archlinux/$repo/os/$arch' ]; then
		sed -i -E '1iServer = https://mirrors.gandi.net/archlinux/$repo/os/$arch' /etc/pacman.d/mirrorlist
	fi

	if [ "$INFOSEC_INSTALL" = "true" ]; then
		if pacman -Qs blackarch-keyring > /dev/null ; then
			echocyan "BlackArch is already enabled. Skipping"
		else
			pacman -Sy --needed --noconfirm --quiet curl
			if ! sh -c "$(curl -fsSL https://blackarch.org/strap.sh)"; then
				echored "Failed installing BlackArch repo. Aborting"
				exit 1
			fi
			sed -i -E '1iServer = https://mirror.cyberbits.eu/blackarch/$repo/os/$arch' /etc/pacman.d/blackarch-mirrorlist
		fi
	fi
	pacman -Syu --needed --noconfirm --quiet --ask 4 $PKG_LIST
	pacman -Scc --noconfirm

	if [ "$SYSTEMD" = true ] && [ -f "/dev/input/mice" ] ; then
		systemctl enable gpm
	fi
	pkgfile -u

	if command -v pikaur >/dev/null 2>&1 ; then
		echocyan "Pikaur is already installed."
	else
		BUILD_DIR=/tmp
		cd $BUILD_DIR
		git clone -q https://aur.archlinux.org/pikaur.git
		git clone -q https://aur.archlinux.org/paru.git
		chown -R nobody:nobody pikaur paru
		cd $BUILD_DIR/pikaur
		HOME=$(pwd) sudo --preserve-env=HOME -u nobody makepkg
		find -name "pikaur*.tar.zst" ! -name "*debug*" -exec pacman --noconfirm -U {} +
		cd $BUILD_DIR/paru
		HOME=$(pwd) sudo --preserve-env=HOME -u nobody makepkg
		find -name "paru*.tar.zst" ! -name "*debug*" -exec pacman --noconfirm -U {} +
		rm -rf $BUILD_DIR/pikaur $BUILD_DIR/paru
	fi

}

arch_user_install()
{
	rm -f ~/.gnupg/public-keys.d/pubring.db.lock
	for key in 2BBED9CB1A68EF55 4A2B758631E1FD91 38EE757D69184620 BE86EBB415104FDF 90ACE942EB90C3DD;
		do gpg --keyserver hkp://keys.gnupg.net --recv-keys $key;
	done
	pikaur -S --noedit --needed --noconfirm $PKG_LIST
	pikaur -Scc --noconfirm
}

centos_install()
{
	dnf install -yq --nogpgcheck epel-release
	echocyan "Upgrading packages."
	dnf upgrade -yq
	echocyan "Installing packages."
	dnf install -yq --nogpgcheck $PKG_LIST
	echocyan "Cleaning."
	dnf clean all

	if [ "$SYSTEMD" = true ]; then
		if [ -f "/dev/input/mice" ]; then
			systemctl enable gpm
		fi
		disable_systemd_service postfix rpcbind.service rpcbind.socket
	fi
	echocyan "End."
}

freebsd_install()
{
	if [ -n "$SET_HOSTNAME" ]; then
		echocyan "Setting hostname to $SET_HOSTNAME"
		echo hostname="$SET_HOSTNAME" >> /etc/rc.conf
	fi

	sed -i -E '/firstboot-growfs/d' /etc/rc.conf
	if ! grep -q "sendmail_enable" /etc/rc.conf ; then
		echo 'sendmail_enable="NONE"' >> /etc/rc.conf
	fi
	pkg update -fq
	pkg upgrade -yq
	pkg install -yq $PKG_LIST
	pkg clean -yqa
}
