#!/bin/sh

set -eo

usage()
{
	cat << EOF >&2
Usage: $0 [-d] [-c] [-h]
	-d	Install desktop environment
	-c	Install custom packages list
	-i	Install infosec tools
	-s	Configure the VM for serial output only
	-h	Display this help and exit
EOF
	exit 1
}

echocyan()
{
	printf "%b" "\033[1;36m$*\033[0m\n"
}
echored()
{
	printf "%b" "\033[1;31m$*\033[0m\n"
}
echoyellow()
{
	printf "%b" "\033[0;33m$*\033[0m\n"
}
echomagenta()
{
	printf "%b" "\033[0;35m$*\033[0m\n"
}

while getopts dcis name
do
	case $name in
	d)
		echomagenta "Will configure dependencies for a desktop environment."
		DESKTOP_INSTALL=true
		;;
	c)
		echomagenta "Using custom install."
		CUSTOM_INSTALL=true
		;;
	i)
		echomagenta "Will install infosec tools."
		INFOSEC_INSTALL=true
		;;
	s)
		echomagenta "Will configure serial output only."
		SERIAL_INSTALL=true
		;;
	?)
		usage
		;;
	esac
done

UNAME_OS=$(uname -o)
UNAME_M=$(uname -m)
SCRIPT_DIR=$(cd "$(dirname "$0")" && pwd)
LIB=$SCRIPT_DIR/lib/unix_install_lib.sh
LIB_DISTRO=$SCRIPT_DIR/lib/unix_distros.sh
DOTFILES_DIR=$SCRIPT_DIR/../dotfiles


case $UNAME_M in
	"x86_64")
		PLATFORM=amd64
		;;
	"amd64")
		PLATFORM=amd64
		;;
	"aarch64")
		PLATFORM=arm64
		;;
	*)
		echored "The platform $UNAME_M is unsupported."
		exit 1
		;;
esac
echocyan "Running on the $PLATFORM platform."

if [ -r "$LIB" ]; then
	. $LIB
else
	echored "Failed reading $LIB. Aborting"
	exit 1
fi

if [ -r "$LIB_DISTRO" ]; then
	. $LIB_DISTRO
else
	echored "Failed reading $LIB_DISTRO. Aborting"
	exit 1
fi

if [ -r /etc/os-release ] ; then
	DISTRO=$(grep "^ID=" /etc/os-release | cut -d = -f 2 | tr -d '"')
	echocyan "Detected the $DISTRO distribution."
else
	echored "Failed auto-detecting the distribution. Aborting."
	exit 1
fi

if [ "$SERIAL_INSTALL" = true ]; then
	echocyan "Setting access for serial console."
	set_console
	if [ "$UNAME_OS" = "GNU/Linux" ]; then
		if command -v grub-mkconfig >/dev/null 2>&1; then
			grub-mkconfig -o /boot/grub/grub.cfg
		elif command -v grub2-mkconfig >/dev/null 2>&1; then
			grub2-mkconfig -o /boot/grub2/grub.cfg
		else
			echo "Did not find the grub-mkconfig executable. Exiting"
			exit 1
		fi
	fi
	echocyan "Successfully set access for the serial console. Exiting"
	exit 0
fi

if [ "$(readlink -f /proc/1/exe | rev | cut -d / -f 1 | rev)" = "systemd" ]; then
	SYSTEMD=true
	echocyan "systemd found"
else
	SYSTEMD=false
fi

if [ -n "$IS_WSL" ] || [ -n "$WSL_DISTRO_NAME" ]; then
	echocyan "Windows Subsystem for Linux environment detected."
	if [ "$SYSTEMD" = true ]; then
		WSLENV=true
	else
		echored "This WSL instance has not been started with systemd. Adding the configuration and exiting the script. Rerun the script after a reboot."
		cat << EOF > /etc/wsl.conf
[boot]
systemd=true
EOF
		exit 1
	fi
else
	WSLENV=false
fi

if [ -f /.dockerenv ] || [ -f /run/.containerenv ] || ( [ -r /proc/1/cgroup ] && cgroup=$(cat /proc/1/cgroup) && [ "${cgroup#*'buildkit'}" != "$cgroup" ] ) ; then
	echocyan "Container environment found"
	CONTAINERENV=true
	USER=$(id -u -n)
	export USER
else
	CONTAINERENV=false
fi

if grep -q '^vagrant:' /etc/passwd ; then
	echocyan "Vagrant environment found"
	VAGRANTENV=true
else
	VAGRANTENV=false
fi



if [ -r "$DOTFILES_DIR" ] && [ -d "$DOTFILES_DIR" ] && [ -x "$DOTFILES_DIR" ] ;
then
	echocyan "Found dotfiles dir at $DOTFILES_DIR."
else
	echored "Failed accessing $DOTFILES_DIR. Aborting."
	exit 1
fi


echocyan "Waiting 5 seconds"
sleep 5


case $DISTRO in
	"debian")
		INSTALL_FUNCTION=debian_install
		;;
	"kali")
		INFOSEC_TOOLS_SCRIPT=$SCRIPT_DIR/lib/infosec_tools.sh
		if [ -r $INFOSEC_TOOLS_SCRIPT ]; then
			# Adding Debian packages as dependencies
			PKG_LIST=$(sed -e '/^[ \t]*#/d' $SCRIPT_DIR/pkglist/debian.txt | xargs)
			INSTALL_FUNCTION=kali_install
		else
			echored "Failed reading $INFOSEC_TOOLS_SCRIPT. Aborting."
			exit 1
		fi
		;;
	"arch")
		if [ "$USER" = "root" ]; then
			if [ "$INFOSEC_INSTALL" = "true" ]; then
				USER_1000=$(grep "1000:1000" /etc/passwd | cut -d : -f 1)
				if [ -z $USER_1000 ]; then
					echored "Did not find a non-privileged user with UID 1000 for this arch installation. Exiting."
					exit 1
				fi
				INFOSEC_TOOLS_SCRIPT=$SCRIPT_DIR/lib/infosec_tools.sh
				if [ ! -r $INFOSEC_TOOLS_SCRIPT ]; then
					echored "Failed reading $INFOSEC_TOOLS_SCRIPT. Aborting."
					exit 1
				fi
			fi
			INSTALL_FUNCTION=arch_install
		else
			INSTALL_FUNCTION=arch_user_install
			USER_SUFFIX="_user"
		fi
		;;
	"almalinux")
		INSTALL_FUNCTION=centos_install
		;;
	"centos")
		INSTALL_FUNCTION=centos_install
		;;
	"freebsd")
		INSTALL_FUNCTION=freebsd_install
		;;
	*)
		echored "The distribution $DISTRO is unsupported."
		exit 1
		;;
esac

PKG_LIST=$(echo $PKG_LIST $(sed -e '/^[ \t]*#/d' $SCRIPT_DIR/pkglist/$DISTRO$USER_SUFFIX.txt | xargs))
if [ "$CUSTOM_INSTALL" = "true" ]; then
	CUSTOM_SUFFIX="_custom"
	if [ -r "$SCRIPT_DIR/pkglist/$DISTRO$USER_SUFFIX$CUSTOM_SUFFIX.txt" ] ; then
		PKG_LIST=$(echo $PKG_LIST $(sed -e '/^[ \t]*#/d' "$SCRIPT_DIR/pkglist/$DISTRO$USER_SUFFIX$CUSTOM_SUFFIX.txt" | xargs))
	else
		echored The file "$SCRIPT_DIR/pkglist/$DISTRO$USER_SUFFIX$CUSTOM_SUFFIX" could not be found. Aborting.
		exit 1
	fi
fi
if [ "$INFOSEC_INSTALL" = "true" ]; then
	INFOSEC_SUFFIX="_infosec"
	if [ -r "$SCRIPT_DIR/pkglist/$DISTRO$USER_SUFFIX$INFOSEC_SUFFIX.txt" ] ; then
		PKG_LIST=$(echo $PKG_LIST $(sed -e '/^[ \t]*#/d' "$SCRIPT_DIR/pkglist/$DISTRO$USER_SUFFIX$INFOSEC_SUFFIX.txt" | xargs))
	else
		echored The file "$SCRIPT_DIR/pkglist/$DISTRO$USER_SUFFIX$INFOSEC_SUFFIX" could not be found. Aborting.
		exit 1
	fi
fi

PKG_LIST=$(echo $PKG_LIST | sed -e "s/amd64/$PLATFORM/")
start_date=$(TZ=UTC date)
echocyan "Started install at $start_date"
eval $INSTALL_FUNCTION

if [ "$USER" = "root" ]; then
	if ! command -v chezmoi >/dev/null 2>&1; then
		sh -c "$(curl -fsLS get.chezmoi.io)" -- -b /usr/local/bin
	fi
	if [ "$UNAME_OS" = "FreeBSD" ]; then
		tzsetup Europe/Paris
	else
		if [ "$SYSTEMD" = true ]; then
			timedatectl set-timezone Europe/Paris
		else
			ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
		fi
	fi

	if [ -w /etc/environment ] && ! grep -q "_JAVA_OPTIONS" /etc/environment
	then
		echo "_JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=lcd -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel'" >> /etc/environment
	fi
	set_locale
	set_services
	set_access

	if [ "$DESKTOP_INSTALL" = true ]; then
		set_desktop
		if command -v grub-mkconfig >/dev/null 2>&1; then
			rm /boot/grub/grubenv
			grub-mkconfig -o /boot/grub/grub.cfg
		elif command -v grub2-mkconfig >/dev/null 2>&1; then
			rm /boot/grub2/grubenv
			grub2-mkconfig -o /boot/grub2/grub.cfg
		else
			echomagenta "Did not find the grub-mkconfig executable."
		fi
	fi

	if ( [ "$DISTRO" = "arch" ] && [ "$INFOSEC_INSTALL" = "true" ] ) || [ "$DISTRO" = "kali" ]; then
		sh $INFOSEC_TOOLS_SCRIPT
	fi

	if [ "$VAGRANTENV" = true ]; then
		echocyan "Setting access for serial console."
		set_console
		if [ "$UNAME_OS" = "GNU/Linux" ]; then
			if command -v grub-mkconfig >/dev/null 2>&1; then
				rm /boot/grub/grubenv
				grub-mkconfig -o /boot/grub/grub.cfg
			elif command -v grub2-mkconfig >/dev/null 2>&1; then
				rm /boot/grub2/grubenv
				grub2-mkconfig -o /boot/grub2/grub.cfg
				grubby --update-kernel=ALL --args="console=tty0 console=hvc0 console=ttyS0,115200"
			else
				echomagenta "Did not find the grub-mkconfig executable."
			fi
		fi
	fi
fi

set_dotfiles

if [ "$USER" = "root" ]; then
	echocyan "Changing default shell to ZSH for \"$USER\""
	chsh -s $(command -v zsh)
	if [ "$UNAME_OS" = "GNU/Linux" ] ; then
		if command -v mandb >/dev/null 2>&1 ; then
			mandb -cq
		fi
		if ! grep PRUNEPATHS /etc/updatedb.conf | grep -q /mnt; then
			sed -i -E '\,PRUNEPATHS,s,"$, /mnt",' /etc/updatedb.conf
		fi
		updatedb
	fi
	if [ "$VAGRANTENV" = true ]; then
		if [ "$UNAME_OS" = "FreeBSD" ]; then
			ifconfig
		else
			ip addr
		fi
		rm -rf /vagrant/*
		rm -rf /dotfiles
		echocyan "Vagrant setup finished. Rebooting..."
		end_date=$(TZ=UTC date)
		echocyan "Installation ended at $end_date"
		reboot
	fi
fi
end_date=$(TZ=UTC date)
echocyan "Installation ended at $end_date"
exit 0
