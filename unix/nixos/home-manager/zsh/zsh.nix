{ config, lib, pkgs, ... }:

{
   home.file = {
    ".config/zsh/p10k.zsh".source = ./p10k.zsh;
    ".config/zsh/p10k_tty.zsh".source = ./p10k_tty.zsh;
  };

  programs.zoxide = {
    enable = true;
    enableZshIntegration = true;
    options = [ "--cmd cd" ];
  };
    programs.fzf = {
    enable = true;
    enableZshIntegration = true;
  };
	programs.zsh = {
		enable = true;
		dotDir = ".config/zsh";
		enableCompletion = false;
		defaultKeymap = "emacs";
		autocd = true;
		history = {
			share = true;
			ignoreSpace = true;
			ignoreDups = true;
			extended = true;
			size = 10000;
#			path = "${config.xdg.dataHome}/zsh/history";
		};
		initExtra = "${builtins.readFile ./config.zsh}";
		plugins = [
			{
			name = "powerlevel10k";
			src = pkgs.zsh-powerlevel10k;
			file = "share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
			}
			{
			name = "fast-syntax-highlighting";
			src = pkgs.zsh-fast-syntax-highlighting;
			file = "share/zsh/site-functions/fast-syntax-highlighting.plugin.zsh";
			}
			{
			name = "autocomplete";
			src = pkgs.zsh-autocomplete;
			file = "share/zsh-autocomplete/zsh-autocomplete.plugin.zsh";
			}
			{
			name = "autosuggestions";
			src = pkgs.zsh-autosuggestions;
			file = "share/zsh-autosuggestions/zsh-autosuggestions.zsh";
			}
			{
			name = "history-search-multi-word";
			src = pkgs.zsh-history-search-multi-word;
			file = "share/zsh/zsh-history-search-multi-word/history-search-multi-word.plugin.zsh";
			}
            #{
			#name = "powerlevel10k-config";
			#src = lib.cleanSource ./. ;
			#file = "p10k.zsh";
			#}
		];
		shellAliases  =  {
			rm = "rm -i";
			mv = "mv -iv";
			cp = "cp -iv";
			ln = "ln -iv";
			mkdir = "mkdir -pv";
			ls = "eza";
			ll = "eza -lhgH --color-scale --git --icons --hyperlink";
			la = "ll -aa";
			laa = "ll -aa@";
			lt = "ll -s time";
			ltr = "ll -s time -r";
			"l." = "ll .* -d";
			dir = "dir --color=auto";
			vdir = "vdir --color=auto";
			grep = "grep --color=auto";
			fgrep = "fgrep --color=auto";
			egrep = "egrep --color=auto";
			ip = "ip -color=auto";
			diff = "diff --color=auto";
			".." = "cd ..";
			"..." = "cd ../../";
			"...." = "cd ../../../";
			"....." = "cd ../../../../";
			".4" = "cd ../../../../";
			".5" = "cd ../../../../..";
			psmem = "ps auxf | sort -nr -k 4";
			psmem10 = "ps auxf | sort -nr -k 4 | head -10";
			pscpu = "ps auxf | sort -nr -k 3";
			pscpu10 = "ps auxf | sort -nr -k 3 | head -10";
			gpumeminfo = "grep -i --color memory /var/log/Xorg.0.log";
			df = "df -H";
			du = "du -ch";
			wget = "wget -c";
			gdb = "gdb -q";
			man = "batman";
			cat = "bat --style=plain --paging=never";
			curl = "curlie";
		};
	};
}
