#!/bin/sh

set -eo

usage()
{
	cat << EOF >&2
Usage: $0 [-g] [-v] [-h]
	-g	Run with Greenbone Vulnerability Manager configuration
	-v	Run with Veil configuration
	-h	Display this help and exit
EOF
	exit 1
}

echocyan()
{
	printf "%b" "\033[1;36m$*\033[0m\n"
}
echored()
{
	printf "%b" "\033[1;31m$*\033[0m\n"
}
echoyellow()
{
	printf "%b" "\033[0;33m$*\033[0m\n"
}
echomagenta()
{
	printf "%b" "\033[0;35m$*\033[0m\n"
}

while getopts gv name
do
	case $name in
	g)
		echomagenta "Will configure Greenbone Vulnerability Manager."
		GVM_SETUP=true
		;;
	v	)
		echomagenta "Will configure Veil."
		VEIL_SETUP=true
		;;
	?)
		usage
		;;
	esac
done

if [ "$(id -u)" -ne 0 ] ; then
	echored "This script must be executed with root privileges. Exiting."
	exit 1
fi

TOOLS_DIR=/tools
SHARED_TOOLS_DIR=$TOOLS_DIR/shared
PUBLIC_DIR=/public

case $(uname -m) in
	"x86_64")
		PLATFORM=amd64
		;;
	"amd64")
		PLATFORM=amd64
		;;
	"aarch64")
		PLATFORM=arm64
		;;
	*)
		echored "The platform $(uname -m) is unsupported. Exiting"
		exit 1
		;;
esac
echocyan "Running on the $PLATFORM platform."

if [ -z "$DISTRO" ]; then
	if [ -r /etc/os-release ] ; then
		DISTRO=$(grep "^ID=" /etc/os-release | cut -d = -f 2 | tr -d '"')
	else
		echored "Failed auto-detecting the distribution. Exiting"
		exit 1
	fi
	if [ $DISTRO = "kali" ] ; then
		echocyan "Running in Kali"
	elif [ $DISTRO = "arch" ] ; then
		echocyan "Running in Arch"
		if ! [ -d /var/lib/postgres/data ]; then
			mkdir -p /var/lib/postgres/data
			chown -R postgres:postgres /var/lib/postgres/data
			chmod 700 /var/lib/postgres/data
		fi
		if [ -n "$(find /var/lib/postgres/data -maxdepth 0 -empty)" ]; then
			echo "/var/lib/postgres/data is empty. Initializing the database."
			runuser -l postgres -c "initdb -D '/var/lib/postgres/data'"
		fi
	else
		echored "Cannot run this script in $DISTRO."
		exit 1
	fi
fi

if [ -f /.dockerenv ] || [ -f /run/.containerenv ] || ( [ -r /proc/1/cgroup ] && cgroup=$(cat /proc/1/cgroup) && [ "${cgroup#*'buildkit'}" != "$cgroup" ] ) ; then
	echocyan "Container environment found"
	CONTAINERENV=true
	USER="$(id -u -n)"
	export USER
else
	CONTAINERENV=false
fi

if [ "$(readlink -f /proc/1/exe | rev | cut -d / -f 1 | rev)" = "systemd" ]; then
	SYSTEMD=true
	echocyan "systemd found"
else
	SYSTEMD=false
fi


unzip_latest()
{
	REPO=$1
	PROJECTNAME=$(echo $REPO | rev | cut -d / -f 1 | rev)
	OUTPUTDIR=$2/$PROJECTNAME
	mkdir -p $OUTPUTDIR
	URL=$(curl -s https://api.github.com/repos/$REPO/releases/latest | grep "browser_download_url.*" | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2 | head -1)
	FILENAME=$(echo $URL | awk -F/ '{print $NF}')
	wget -nv $URL -P $OUTPUTDIR
	unzip $OUTPUTDIR/$FILENAME -d $OUTPUTDIR
	rm -rf "${OUTPUTDIR:?}/$FILENAME"
}

download_latest()
{
	REPO=$1
	OUTPUTDIR=$2
	URL=$(curl -s https://api.github.com/repos/$REPO/releases/latest | grep "browser_download_url.*" | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2 | head -1)
	wget -nv $URL -P $OUTPUTDIR
}

get_latest_gist()
{
	ID=$1
	curl -s https://api.github.com/gists/$ID | grep "raw_url" | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2 | head -1 | tr -d ,
}

install_python_script()
{
	python_script_path=$1
	python_script=$(echo $python_script_path | rev | cut -d / -f 1 | rev)
	if [ ! "$python_script_path" = "${python_script_path#https:\/\/}" ]; then
		if [ "$#" -ne "2" ]; then
			echored "You must provide two arguments when using an URL."
			echored "Usage: $0 <URL> <download directory>"
			exit 1
		fi
		output_dir=$2
		wget -nv $python_script_path -P $output_dir
		python_script_path=$output_dir/$python_script
	fi
	dos2unix $python_script_path
	sed -i -E '1 s/python$/python3/' $python_script_path
	python_script_head=$(head -1 $python_script_path)
	if [ "$python_script_head" = "${python_script_head#\#\!/}" ]; then
		echoyellow Missing the shebang in $python_script_path. Adding it manually.
		sed -i -E '1i #!/usr/bin/env python3' $python_script_path
	fi
	chmod +x $python_script_path
	python_script_noext=$(echo $python_script | rev | cut -d . -f 2- | rev | tr '[:upper:]' '[:lower:]')
	ln -sf $python_script_path /usr/local/bin/$python_script_noext
}

if [ "$CONTAINERENV" = true ]; then
	[ -r "/lib/systemd/system/postgresql.service" ] && [ ! -e "/etc/systemd/system/multi-user.target.wants/postgresql.service" ] && mkdir -p /etc/systemd/system/multi-user.target.wants && ln -sf /lib/systemd/system/postgresql.service /etc/systemd/system/multi-user.target.wants/postgresql.service
	if [ -x "$(command -v service)" ]; then
		service postgresql start
	fi
elif [ "$SYSTEMD" = true ]; then
	systemctl enable postgresql.service
	systemctl start postgresql.service
else
	echored 'The commands "service" and "systemd" are not available. Exiting'
	exit 1
fi

if [ -d $TOOLS_DIR ]; then
	echocyan "$TOOLS_DIR already exists, Deleting..."
	rm -rf $TOOLS_DIR
fi

export PYTHON_SITE_PACKAGE=$(python -c 'import site; print(site.getsitepackages()[0])')
find $PYTHON_SITE_PACKAGE/certipy_ad* -name "requires.txt" -exec sed -i -E "/pyasn1/s/0.6.0/0.10.0/" {} \;

echo "PIPX_HOME=/opt/pipx" >> /etc/environment
echo "PIPX_BIN_DIR=/usr/local/bin" >> /etc/environment
export PIPX_HOME=/opt/pipx
export PIPX_BIN_DIR=/usr/local/bin


echocyan "Downloading tools to $TOOLS_DIR"
find -L /usr/local/bin -type l -delete
mkdir -p $SHARED_TOOLS_DIR
mkdir -p $PUBLIC_DIR
chmod 777 $PUBLIC_DIR
if grep -q nobody /etc/group ; then
	chown nobody:nobody $PUBLIC_DIR
else
	chown nobody:nogroup $PUBLIC_DIR
fi


if [ $DISTRO = "kali" ]; then
	if [ "$PLATFORM" = "amd64" ]; then
		touch ~/.hushlogin

		LATEST_BURP_VERSION=$(curl --silent https://portswigger.net/burp/releases --stderr - | grep "Professional / Community" | head -1 | sed -e 's/<[^>]*>//g' | awk '{$1=$1};1' | cut -d ' ' -f 4 | tr -d '\r')
		echocyan "Installing Burp Suite Professional $LATEST_BURP_VERSION."
		BURP_PRO_URL="https://portswigger.net/burp/releases/download?product=pro&type=Linux&version=$LATEST_BURP_VERSION"
		BURP_PRO_INSTALL=$(curl -s -I $BURP_PRO_URL | grep -i Content-Disposition| cut -d ' ' -f 3 | cut -d '=' -f 2| cut -d ';' -f 1 | tr -d "\r")
		if [ -n "$BURP_PRO_INSTALL" ]; then
			wget --content-disposition -nv $BURP_PRO_URL -P /tmp
			chmod +x /tmp/$BURP_PRO_INSTALL
			_JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=lcd -Dswing.aatext=true' /tmp/$BURP_PRO_INSTALL -q
			rm -rf /tmp/$BURP_PRO_INSTALL
			PWNFOX_URL=$(curl -s https://api.github.com/repos/yeswehack/PwnFox/releases/latest | grep "browser_download_url.*" | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
			wget -nv $PWNFOX_URL -P /usr/share/java
			mkdir -p /usr/share/applications/
			ln -sf "/opt/BurpSuitePro/Burp Suite Professional.desktop" /usr/share/applications/
		else
			echored "Failed installing $BURP_PRO_INSTALL. Aborting."
			exit 1
		fi

		KERBRUTE_URL=$(curl -s https://api.github.com/repos/ropnop/kerbrute/releases/latest | grep "browser_download_url.*" | grep linux_$PLATFORM | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
		wget -nv $KERBRUTE_URL -O $TOOLS_DIR/kerbrute
		chmod +x $TOOLS_DIR/kerbrute
		ln -sf $TOOLS_DIR/kerbrute /usr/local/bin

		wget -nv https://github.com/ropnop/go-windapsearch/releases/latest/download/windapsearch-linux-amd64 -O $TOOLS_DIR/windapsearch
		chmod +x $TOOLS_DIR/windapsearch
		ln -sf $TOOLS_DIR/windapsearch /usr/local/bin

		WEZTERM_URL=$(curl -s https://api.github.com/repos/wez/wezterm/releases/latest | grep "browser_download_url.*" | grep Ubuntu22.04.deb\" | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
		wget -nv $WEZTERM_URL -O /tmp/wezterm.deb
		dpkg -i /tmp/wezterm.deb
		rm -rf /tmp/wezterm.deb
		ln -sf /usr/share/pspy $SHARED_TOOLS_DIR
	else
		wget -nv https://github.com/DominicBreuker/pspy/releases/latest/download/pspy64 -P $SHARED_TOOLS_DIR
		chmod +x $SHARED_TOOLS_DIR/pspy64
	fi

	AQUATONE_URL=$(curl -s https://api.github.com/repos/michenriksen/aquatone/releases/latest | grep "browser_download_url.*" | grep linux | grep $PLATFORM | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
	wget -qO- $AQUATONE_URL | bsdtar -xvf- -C $SHARED_TOOLS_DIR aquatone
	ln -sf $SHARED_TOOLS_DIR/aquatone /usr/local/bin

	ln -sf /usr/share/seclists/Passwords/Leaked-Databases/rockyou.txt /usr/share/wordlists/rockyou.txt
	rm -rf /usr/share/wordlists/rockyou.txt.gz

	PHP_VERSION=$(php -v | grep built |cut -d ' ' -f 2 | cut -d . -f 1,2)
	sed -i -E "/post_max_size/s/[0-9]\+M/$PHP_UPLOAD_SIZE/" "/etc/php/$PHP_VERSION/apache2/php.ini"
	sed -i -E "/upload_max_filesize/s/[0-9]\+M/$PHP_UPLOAD_SIZE/" "/etc/php/$PHP_VERSION/apache2/php.ini"

	for script in /usr/share/creddump7/*.py ; do install_python_script $script ; done

	git clone -q https://github.com/rbsec/dnscan.git $TOOLS_DIR/dnscan
	install_python_script $TOOLS_DIR/dnscan/dnscan.py
	git clone -q https://github.com/chinarulezzz/spoofcheck.git $TOOLS_DIR/spoofcheck
	install_python_script $TOOLS_DIR/spoofcheck/spoofcheck.py
	git clone -q https://github.com/nccgroup/demiguise $TOOLS_DIR/demiguise
	install_python_script $TOOLS_DIR/demiguise/demiguise.py

	install_python_script https://raw.githubusercontent.com/secretsquirrel/SigThief/master/sigthief.py $TOOLS_DIR
	install_python_script https://raw.githubusercontent.com/blunderbuss-wctf/wacker/master/wacker.py $TOOLS_DIR
	install_python_script https://raw.githubusercontent.com/lgandx/PCredz/master/Pcredz $TOOLS_DIR

	wget -nv https://download.sysinternals.com/files/SysinternalsSuite.zip -P $SHARED_TOOLS_DIR/sysinternals-suite
	unzip $SHARED_TOOLS_DIR/sysinternals-suite/SysinternalsSuite.zip -d $SHARED_TOOLS_DIR/sysinternals-suite
	rm -rf $SHARED_TOOLS_DIR/sysinternals-suite/Eula.txt $SHARED_TOOLS_DIR/sysinternals-suite/SysinternalsSuite.zip

	unzip_latest 'netwrix/pingcastle' $SHARED_TOOLS_DIR

	if [ ! "$PLATFORM" = "amd64" ]; then
		ADALANCHE_URL=$(curl -s https://api.github.com/repos/lkarlslund/Adalanche/releases/latest | grep "browser_download_url.*" | grep linux-$PLATFORM | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
	else
		ADALANCHE_URL=$(curl -s https://api.github.com/repos/lkarlslund/Adalanche/releases/latest | grep "browser_download_url.*" | grep linux-x64 | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
	fi

	wget -nv $ADALANCHE_URL -O $TOOLS_DIR/adalanche
	chmod +x $TOOLS_DIR/adalanche
	ln -sf $TOOLS_DIR/adalanche /usr/local/bin

	wget -nv https://github.com/rebootuser/LinEnum/raw/master/LinEnum.sh -P $SHARED_TOOLS_DIR
	wget -nv https://raw.githubusercontent.com/diego-treitos/linux-smart-enumeration/master/lse.sh -P $SHARED_TOOLS_DIR
	git clone -q https://github.com/dirkjanm/krbrelayx.git $TOOLS_DIR/krbrelayx
	for file in $TOOLS_DIR/krbrelayx/*.py ; do install_python_script $file ; done
	git clone -q https://github.com/dirkjanm/PKINITtools.git $TOOLS_DIR/PKINITtools
	for file in $TOOLS_DIR/PKINITtools/*.py ; do install_python_script $file ; done

	wget -nv https://github.com/shadow1ng/fscan/releases/latest/download/fscan -O $SHARED_TOOLS_DIR/fscan
	chmod +x $SHARED_TOOLS_DIR/fscan
	ln -sf $SHARED_TOOLS_DIR/fscan /usr/local/bin

	mkdir -p /usr/share/fonts/truetype/meslo
	wget -nv https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf -P /usr/share/fonts/truetype/meslo
	wget -nv https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf -P /usr/share/fonts/truetype/meslo
	wget -nv https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf -P /usr/share/fonts/truetype/meslo
	wget -nv https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf -P /usr/share/fonts/truetype/meslo

	git clone -q https://github.com/outflanknl/EvilClippy.git $SHARED_TOOLS_DIR/EvilClippy
	cd $SHARED_TOOLS_DIR/EvilClippy
	mcs /reference:OpenMcdf.dll,System.IO.Compression.FileSystem.dll /out:EvilClippy.exe *.cs
	cd -

	ln -sf /usr/lib/bloodhound/resources/app/Collectors/SharpHound* $SHARED_TOOLS_DIR
	ln -sf /usr/share/linux-exploit-suggester/linux-exploit-suggester.sh $SHARED_TOOLS_DIR
	ln -sf /usr/share/peass $SHARED_TOOLS_DIR
	ln -sf /usr/share/windows-resources $SHARED_TOOLS_DIR
	ln -sf /usr/share/unix-privesc-check/unix-privesc-check $SHARED_TOOLS_DIR
	ln -sf /usr/bin/ligolo-agent $SHARED_TOOLS_DIR
	ln -sf /usr/bin/ligolo-proxy $SHARED_TOOLS_DIR
	ln -sf /usr/share/powershell-empire/empire/server/data/module_source $SHARED_TOOLS_DIR/empire
	wget -nv https://raw.githubusercontent.com/zcgonvh/EfsPotato/master/EfsPotato.cs -P $SHARED_TOOLS_DIR
	mono-csc $SHARED_TOOLS_DIR/EfsPotato.cs -nowarn:1691,618 /out:$SHARED_TOOLS_DIR/EfsPotato.exe
	rm -rf $SHARED_TOOLS_DIR/EfsPotato.cs
	mkdir -p /var/www
	HTTP_DIR=/var/www/html
	APACHE_CONF_FILE="/etc/apache2/apache2.conf"
	msfdb reinit --use-defaults

	gem install zsteg one_gadget
	for package in aclpwn pwncat-cs droopescan apachetomcatscanner krbjack frida-tools git+https://github.com/franc-pentest/ldeep smbclientng corscanner git+https://github.com/Synacktiv/eos git-dumper wesng arsenal-cli ; do
		pipx install $package
	done
	pip3 install --break-system-packages argparse keystone-engine py-emailprotections python-libpcap
	PYTHON_SITE_PACKAGE=$(python3 -c 'import site; print(site.getsitepackages()[0])')
	#sed -i -E "s/neo4j.v1/neo4j/" $PYTHON_SITE_PACKAGE/aclpwn/database.py

	GOBIN=$TOOLS_DIR go install github.com/xm1k3/cent@latest
	ln -sf $TOOLS_DIR/cent /usr/local/bin
elif [ $DISTRO = "arch" ]; then
	ln -sf /usr/bin/ligolo-ng-proxy $SHARED_TOOLS_DIR
	ln -sf /usr/bin/ligolo-ng-agent $SHARED_TOOLS_DIR

	sed -i -E "/post_max_size/s/[0-9]\+M/$PHP_UPLOAD_SIZE/" "/etc/php/php.ini"
	sed -i -E "/upload_max_filesize/s/[0-9]\+M/$PHP_UPLOAD_SIZE/" "/etc/php/php.ini"
	wesng --update
	ln -sf /usr/share/bloodhound/Collectors/SharpHound* $SHARED_TOOLS_DIR
	ln -sf /usr/share/windows/ $SHARED_TOOLS_DIR
	ln -sf /usr/share/linenum/LinEnum.sh $SHARED_TOOLS_DIR
	ln -sf /usr/share/linux-smart-enumeration/lse.sh $SHARED_TOOLS_DIR
	ln -sf /usr/share/linux-exploit-suggester.sh/linux-exploit-suggester.sh $SHARED_TOOLS_DIR
	ln -sf /usr/share/peass/linPEAS $SHARED_TOOLS_DIR
	ln -sf /usr/bin/unix-privesc-check $SHARED_TOOLS_DIR
	ln -sf /usr/bin/aquatone $SHARED_TOOLS_DIR
	ln -sf /usr/share/evilclippy $SHARED_TOOLS_DIR
	ln -sf /usr/bin/pspy $SHARED_TOOLS_DIR
	ln -sf /usr/share/empire/empire/server/data/module_source $SHARED_TOOLS_DIR/empire
	wget -nv https://raw.githubusercontent.com/zcgonvh/EfsPotato/master/EfsPotato.cs -P $SHARED_TOOLS_DIR
	csc $SHARED_TOOLS_DIR/EfsPotato.cs -nowarn:1691,618 /out:$SHARED_TOOLS_DIR/EfsPotato.exe
	rm -rf $SHARED_TOOLS_DIR/EfsPotato.cs

	HTTP_DIR=/srv/http
	APACHE_CONF_FILE="/etc/httpd/conf/httpd.conf"
fi

mkdir -p $TOOLS_DIR/cent-nuclei-templates
cent init --overwrite
cent -p $TOOLS_DIR/cent-nuclei-templates

mkdir -p $TOOLS_DIR/BloodHound
wget -nv https://raw.githubusercontent.com/SpecterOps/bloodhound/main/examples/docker-compose/docker-compose.yml -P $TOOLS_DIR/BloodHound
echo 'BLOODHOUND_PORT=38080' > $TOOLS_DIR/BloodHound/.env
SHARPHOUND_URL=$(curl -s https://api.github.com/repos/BloodHoundAD/SharpHound/releases/latest | grep "browser_download_url.*" | grep -v debug | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
curl -L -s -o - $SHARPHOUND_URL | bsdtar -xvf- -C $TOOLS_DIR/BloodHound SharpHound.exe
curl -L -s -o - $SHARPHOUND_URL | bsdtar -xvf- -C $TOOLS_DIR/BloodHound SharpHound.ps1
ln -sf $TOOLS_DIR/BloodHound/SharpHound.exe $SHARED_TOOLS_DIR/SharpHound2.exe
ln -sf $TOOLS_DIR/BloodHound/SharpHound.ps1 $SHARED_TOOLS_DIR/SharpHound2.ps1

AZUREHOUND_WIN64_URL=$(curl -s https://api.github.com/repos/BloodHoundAD/AzureHound/releases/latest | grep "browser_download_url.*" | grep -v sha256 | grep windows-amd64 | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
curl -L -s -o - $AZUREHOUND_WIN64_URL | bsdtar -xvf- -C $TOOLS_DIR/BloodHound azurehound.exe
ln -sf $TOOLS_DIR/BloodHound/azurehound.exe $SHARED_TOOLS_DIR/azurehound.exe

AZUREHOUND_LIN64_URL=$(curl -s https://api.github.com/repos/BloodHoundAD/AzureHound/releases/latest | grep "browser_download_url.*" | grep -v sha256 | grep linux-amd64 | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
curl -L -s -o - $AZUREHOUND_LIN64_URL | bsdtar -xvf- -C $TOOLS_DIR/BloodHound azurehound
chmod +x $TOOLS_DIR/BloodHound/azurehound

# https://raw.githubusercontent.com/ZeroPointSecurity/RTOVMSetup/master/kali.sh
install_python_script "$(get_latest_gist 11076951)" $TOOLS_DIR
git clone -q https://github.com/FortyNorthSecurity/Egress-Assess.git $TOOLS_DIR/Egress-Assess
install_python_script $TOOLS_DIR/Egress-Assess/Egress-Assess.py
ln -sf $TOOLS_DIR/Egress-Assess/EgressAssess.ps1 $SHARED_TOOLS_DIR
git clone -q https://github.com/plackyhacker/Shellcode-Encryptor.git $TOOLS_DIR/Shellcode-Encryptor
install_python_script $TOOLS_DIR/Shellcode-Encryptor/shellcode_encryptor.py
git clone -q https://github.com/SYANiDE-/SuperSharpShooter $TOOLS_DIR/SuperSharpShooter
install_python_script $TOOLS_DIR/SuperSharpShooter/SuperSharpShooter.py
git clone -q https://github.com/rvrsh3ll/FindFrontableDomains.git $TOOLS_DIR/FindFrontableDomains
install_python_script $TOOLS_DIR/FindFrontableDomains/FindFrontableDomains.py
git clone -q --recurse-submodules https://github.com/FSecureLABS/physmem2profit.git $TOOLS_DIR/physmem2profit
git clone -q https://github.com/Tylous/Freeze.git $TOOLS_DIR/Freeze
cd $TOOLS_DIR/Freeze
go build Freeze.go
cd -
ln -sf $TOOLS_DIR/Freeze/Freeze /usr/local/bin
git clone -q https://github.com/chenjj/espoofer.git $TOOLS_DIR/espoofer
install_python_script $TOOLS_DIR/espoofer/espoofer.py
git clone -q https://github.com/s0md3v/Corsy.git $TOOLS_DIR/Corsy
install_python_script $TOOLS_DIR/Corsy/corsy.py
git clone -q https://github.com/synacktiv/SCCMSecrets.git $TOOLS_DIR/SCCMSecrets
install_python_script $TOOLS_DIR/SCCMSecrets/SCCMSecrets.py

wget -nv https://raw.githubusercontent.com/itm4n/PrivescCheck/master/PrivescCheck.ps1 -P $SHARED_TOOLS_DIR
wget -nv https://raw.githubusercontent.com/itm4n/PrivescCheck/refs/heads/master/src/exploit/PointAndPrint.ps1 -P $SHARED_TOOLS_DIR
wget -nv https://raw.githubusercontent.com/dafthack/HostRecon/master/HostRecon.ps1 -P $SHARED_TOOLS_DIR

install_python_script https://raw.githubusercontent.com/NotMedic/NetNTLMtoSilverTicket/master/dementor.py $TOOLS_DIR
install_python_script https://raw.githubusercontent.com/topotam/PetitPotam/main/PetitPotam.py $TOOLS_DIR
install_python_script https://raw.githubusercontent.com/Wh04m1001/DFSCoerce/main/dfscoerce.py $TOOLS_DIR
install_python_script https://raw.githubusercontent.com/ShutdownRepo/ShadowCoerce/main/shadowcoerce.py $TOOLS_DIR
install_python_script https://raw.githubusercontent.com/dirkjanm/PrivExchange/master/privexchange.py $TOOLS_DIR
install_python_script https://raw.githubusercontent.com/SecuraBV/CVE-2020-1472/master/zerologon_tester.py $TOOLS_DIR
install_python_script https://raw.githubusercontent.com/Mr-Un1k0d3r/SCShell/master/scshell.py $TOOLS_DIR
install_python_script https://raw.githubusercontent.com/sosdave/KeyTabExtract/master/keytabextract.py $TOOLS_DIR
install_python_script https://raw.githubusercontent.com/t3l3machus/psudohash/main/psudohash.py $TOOLS_DIR
install_python_script https://raw.githubusercontent.com/grimlockx/ADCSKiller/main/adcskiller.py $TOOLS_DIR
install_python_script https://raw.githubusercontent.com/irsl/curlshell/main/curlshell.py $TOOLS_DIR

wget -nv https://github.com/topotam/PetitPotam/raw/main/PetitPotam.exe -P $SHARED_TOOLS_DIR
wget -nv https://raw.githubusercontent.com/NetSPI/PowerUpSQL/master/PowerUpSQL.ps1 -P $SHARED_TOOLS_DIR
wget -nv https://raw.githubusercontent.com/leoloobeek/LAPSToolkit/master/LAPSToolkit.ps1 -P $SHARED_TOOLS_DIR
wget -nv https://github.com/SnaffCon/Snaffler/releases/latest/download/Snaffler.exe -P $SHARED_TOOLS_DIR
wget -nv https://github.com/GossiTheDog/HiveNightmare/raw/master/Release/HiveNightmare.exe -P $SHARED_TOOLS_DIR
wget -nv https://github.com/shadow1ng/fscan/releases/latest/download/fscan.exe -P $SHARED_TOOLS_DIR
wget -nv https://github.com/wh0amitz/PetitPotato/releases/latest/download/PetitPotato.exe -P $SHARED_TOOLS_DIR
wget -nv https://github.com/BeichenDream/GodPotato/releases/latest/download/GodPotato-NET4.exe -O $SHARED_TOOLS_DIR/GodPotato.exe

# git clone -q https://github.com/r3motecontrol/Ghostpack-CompiledBinaries $SHARED_TOOLS_DIR/GhostPack
git clone -q https://github.com/F4l13n5n0w/PowerSharpPack.git $SHARED_TOOLS_DIR/PowerSharpPack
git clone -q https://github.com/Flangvik/SharpCollection.git $SHARED_TOOLS_DIR/SharpCollection
CHISEL_WIN64_URL=$(curl -s https://api.github.com/repos/jpillora/chisel/releases/latest | grep "browser_download_url.*" | grep windows_amd64.gz | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
CHISEL_LINUX64_URL=$(curl -s https://api.github.com/repos/jpillora/chisel/releases/latest | grep "browser_download_url.*" | grep linux_amd64.gz | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
curl -L -s -o - $CHISEL_WIN64_URL | gunzip > $SHARED_TOOLS_DIR/chisel.exe
curl -L -s -o - $CHISEL_LINUX64_URL | gunzip > $SHARED_TOOLS_DIR/chisel

LAZAGNE_URL=$(curl -s https://api.github.com/repos/AlessandroZ/LaZagne/releases/latest | grep "browser_download_url.*" | grep .exe | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
wget -nv $LAZAGNE_URL -P $SHARED_TOOLS_DIR

GOBUSTER_WIN64_URL=$(curl -s https://api.github.com/repos/OJ/gobuster/releases/latest | grep "browser_download_url.*" | grep "Windows_x86_64" | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
wget -qO- $GOBUSTER_WIN64_URL | bsdtar -xvf- -C $SHARED_TOOLS_DIR gobuster.exe

ADALANCHE_COLLECTOR_URL=$(curl -s https://api.github.com/repos/lkarlslund/Adalanche/releases/latest | grep "browser_download_url.*" | grep collector-windows | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
wget -nv $ADALANCHE_COLLECTOR_URL -P $SHARED_TOOLS_DIR

AQUATONE_WIN_URL=$(curl -s https://api.github.com/repos/michenriksen/aquatone/releases/latest | grep "browser_download_url.*" | grep windows | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
wget -qO- $AQUATONE_WIN_URL | bsdtar -xvf- -C $SHARED_TOOLS_DIR aquatone.exe

if [ "$PLATFORM" = "amd64" ]; then
	SCARECROW_URL=$(curl -s https://api.github.com/repos/optiv/ScareCrow/releases/latest | grep "browser_download_url.*" | grep linux_amd64 | cut -d : -f 2,3 | tr -d \" | cut -d " " -f 2)
	wget -nv $SCARECROW_URL -O $TOOLS_DIR/scarecrow
	chmod +x $TOOLS_DIR/scarecrow
	ln -sf $TOOLS_DIR/scarecrow /usr/local/bin
fi

mkdir -p $SHARED_TOOLS_DIR/amsi
wget -nv https://raw.githubusercontent.com/pracsec/AmsiBypassHookManagedAPI/main/src/PowerShell/BypassAddType.ps1 -P $SHARED_TOOLS_DIR/amsi

wget -nv https://raw.githubusercontent.com/S3cur3Th1sSh1t/Amsi-Bypass-Powershell/master/README.md -P $SHARED_TOOLS_DIR/amsi
awk -v RS='```' -v outputdir="$SHARED_TOOLS_DIR/amsi/" 'NR % 2 == 0 { print $0 > (outputdir "bypass" ++count ".ps1") }' $SHARED_TOOLS_DIR/amsi/README.md
for file in $SHARED_TOOLS_DIR/amsi/*.ps1 ; do
	sed -i '1{/^powershell$/d}' "$file"
	sed -i -e '/./,$!d' -e :a -e '/^\n*$/{$d;N;ba' -e '}' "$file";
done


PHP_UPLOAD_SIZE="50M"
pip3 install --break-system-packages uploadserver
for package in git+https://github.com/Orange-Cyberdefense/graphcat git+https://github.com/dadevel/mssql-spider.git@main postmaniac msi-utils GoldenCopy git+https://github.com/Hackndo/WebclientServiceScanner git+https://github.com/p0dalirius/GeoWordlists git+https://github.com/ScorpionesLabs/MSSqlPwner ; do
	pipx install $package
done
install_python_script /usr/local/bin/graphcat.py

tar xzf /usr/share/seclists/Passwords/Leaked-Databases/rockyou.txt.tar.gz -C /usr/share/seclists/Passwords/Leaked-Databases/
wget -nv https://raw.githubusercontent.com/stealthsploit/OneRuleToRuleThemStill/main/OneRuleToRuleThemStill.rule -P $TOOLS_DIR

if ! grep -q "legacy_sect" /etc/ssl/openssl.cnf ; then
	echocyan "Enabling legacy OpenSSL algorithms."
	if grep -q "CipherString" /etc/ssl/openssl.cnf ; then
		sed -i -E '/CipherString = DEFAULT@SECLEVEL=2/s/^/#/' /etc/ssl/openssl.cnf
	else
		echo 'CipherString = DEFAULT@SECLEVEL=2' >> /etc/ssl/openssl.cnf
	fi
	sed -i -E '/default = default_sect/alegacy = legacy_sect' /etc/ssl/openssl.cnf
	sed -i -E '/activate = 1/s/^#//' /etc/ssl/openssl.cnf
	printf "[legacy_sect]\nactivate = 1" >> /etc/ssl/openssl.cnf
fi

rm -rf $HTTP_DIR
ln -sf $SHARED_TOOLS_DIR $HTTP_DIR
ln -sf $PUBLIC_DIR $SHARED_TOOLS_DIR/uploads
wget -nv https://gist.githubusercontent.com/taterbase/2688850/raw/b9d214c9cbcf624e13c825d4de663e77bf38cc14/upload.php -P $HTTP_DIR


USER_1000=$(grep "1000:1000" /etc/passwd | cut -d : -f 1)
if [ -n "$USER_1000" ]; then
	echocyan "Setting infosec tools for $USER_1000"
	if [ $DISTRO = "arch" ]; then
		runuser -l $USER_1000 -c "msfdb init --use-defaults"
	fi
	su - $USER_1000 -c 'mkdir -p ~/.ZAP ~/.config/bloodhound ;\
	rm -rf ~/.ZAP/dirbuster ;\
	ln -sf /usr/share/seclists/Discovery/Web-Content ~/.ZAP/dirbuster ;\
	wget -nv https://raw.githubusercontent.com/ZephrFish/Bloodhound-CustomQueries/main/customqueries.json -P ~/.config/bloodhound'
	chown -R $USER_1000:$USER_1000 $TOOLS_DIR
	if grep -q vbosf /etc/group; then
		usermod -a -G vboxsf $USER_1000
	fi
	if [ ! "$(systemd-detect-virt)" = "none" ] && [ -w "/etc/lightdm/lightdm.conf" ]; then
		sed -i -E "/#autologin-user=/s/$/$USER_1000/" /etc/lightdm/lightdm.conf
		sed -i -E "/#autologin-user=/s/^#//" /etc/lightdm/lightdm.conf
	fi
	if [ "$(systemd-detect-virt)" = "kvm" ] && [ "$(systemctl -M $USER_1000\@ --user list-unit-files 'plasma-core.target' | wc -l)" -gt 3 ] ; then
		echocyan "Adding a custom service for spice-vdagent for KDE windows resizing."
		su - $USER_1000 -c 'mkdir -p ~/.config/systemd/user'
		su - $USER_1000 -c 'cat << EOF > ~/.config/systemd/user/spice-vdagent.service
[Unit]
Description=Spice guest agent
After=plasma-core.target

[Service]
ExecStart=/usr/bin/spice-vdagent -x

[Install]
WantedBy=plasma-core.target
EOF'
		systemctl daemon-reload
		systemctl -M $USER_1000\@ --user enable spice-vdagent.service
	fi
else
	echoyellow "Failed getting a user with UID 1000."
fi

if [ -d "/usr/share/neo4j" ] && [ ! -d "/usr/share/neo4j/run" ]; then
	mkdir /usr/share/neo4j/run
fi

rm -rf /srv/tftp
ln -sf $PUBLIC_DIR /srv/tftp
if [ -w /etc/default/tftpd-hpa ]; then
	sed -i -E "/TFTP_OPTIONS/s/--secure/--secure --create/" /etc/default/tftpd-hpa
elif [ -w /etc/conf.d/tftpd ]; then
	sed -i -E "/TFTPD_ARGS/s/--secure/--secure --create/" /etc/conf.d/tftpd
fi

if [ -w /etc/proxychains4.conf ]; then
	sed -i -E "s/socks4 	127.0.0.1 9050/socks5 127.0.0.1 1080/" /etc/proxychains4.conf
elif [ -w /etc/proxychains.conf ]; then
	sed -i -E "s/socks4 	127.0.0.1 9050/socks5 127.0.0.1 1080/" /etc/proxychains.conf
fi

if [ -w /etc/vsftpd.conf ]; then
	if grep -q 'seccomp_sandbox' /etc/vsftpd.conf ; then
		echoyellow vsFTPd is already configured.
	else
		sed -i -E "/anonymous_enable/s/NO/YES/" /etc/vsftpd.conf
		sed -i -E "/anon_upload_enable/s/NO/YES/" /etc/vsftpd.conf
		sed -i -E "/anon_upload_enable/s/^#//" /etc/vsftpd.conf
		sed -i -E "/write_enable/s/^#//" /etc/vsftpd.conf
		sed -i -E "/local_enable/s/^#//" /etc/vsftpd.conf
		echo "seccomp_sandbox=NO" >> /etc/vsftpd.conf
		echo "no_anon_password=YES" >> /etc/vsftpd.conf
	fi
fi

if [ -w "/etc/samba/smb.conf" ]; then
	if grep -q "$SHARED_TOOLS_DIR" /etc/samba/smb.conf ; then
		echoyellow Samba is already configured
	else
	sed -i -E '/\[global\]/aunix extensions = no' /etc/samba/smb.conf
	cat << EOF >> /etc/samba/smb.conf
[tools]
	path = $SHARED_TOOLS_DIR
	writable = no
	guest ok = yes
	guest only = yes
	read only = yes
	directory mode = 0555
	browseable = yes
	follow symlinks = yes
	wide links = yes

[public]
	path = $PUBLIC_DIR
	writable = yes
	guest ok = yes
	guest only = yes
	read only = no
	browseable = no
	follow symlinks = no
	wide links = no
EOF
		echocyan "Successfully configured Samba"
	fi
else
	echoyellow "Cannot write to Samba configuration file. Skipping."
fi


if [ -w $APACHE_CONF_FILE ] ; then
	if grep -q "$HTTP_DIR/uploads" $APACHE_CONF_FILE ; then
		echoyellow "Apache already configured"
	else
		cat << EOF >> $APACHE_CONF_FILE
<Directory $HTTP_DIR/uploads>
	<Files "*.*">
		SetHandler !
	</Files>
</Directory>
EOF
		if [ $DISTRO = "arch" ]; then
			sed -i -E '/mod_proxy.so$/s/^#//' $APACHE_CONF_FILE
			sed -i -E '/mod_proxy_fcgi.so$/s/^#//' $APACHE_CONF_FILE
			if grep -q "extra/php-fpm.conf" $APACHE_CONF_FILE ; then
				echo "Include conf/extra/php-fpm.conf" >> $APACHE_CONF_FILE
				cat << EOF > /etc/httpd/conf/extra/php-fpm.conf
DirectoryIndex index.php index.html
<FilesMatch \.php$>
	SetHandler "proxy:unix:/run/php-fpm/php-fpm.sock|fcgi://localhost/"
</FilesMatch>
EOF
			fi
		fi
		echocyan "Successfully configured Apache"
	fi
else
	echoyellow "Cannot write to Apache configuration file. Skipping."
fi

if [ -x "$(command -v gvm-setup)" ] && [ "$GVM_SETUP" = "true" ] ; then
	echocyan "Configuring Greenbone Vulnerability Manager (this will take a while...)"
	gvm-setup
	echocyan "Setting GVM password to $(hostname)"
	gvmd --user=admin --new-password="$(hostname)"
	if [ -f "/lib/systemd/system/greenbone-security-assistant.service" ]; then
		sed -i -E 's/127.0.0.1/0.0.0.0/g' /lib/systemd/system/greenbone-security-assistant.service
		if grep -v -- "--allow-header-host" /lib/systemd/system/greenbone-security-assistant.service ; then
			sed -i -E "/ExecStart=/s/$/ --no-redirect --allow-header-host $(hostname)/" /lib/systemd/system/greenbone-security-assistant.service
		fi
	fi
	echocyan "Sucessfully configured Greenbone Vulnerability Manager."
fi

if [ -f /usr/share/veil/config/setup.sh ] && [ "$VEIL_SETUP" = "true" ]; then
	if [ $DISTRO = "kali" ]; then
		sed -i -E '/drive_c/s/pefile/-Iv" "pefile==2019.4.18/' /usr/share/veil/config/setup.sh
	fi
	/usr/share/veil/config/setup.sh --force --silent
fi