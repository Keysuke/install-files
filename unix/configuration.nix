# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      <home-manager/nixos>
    ];

  # Bootloader.
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/vda";
  boot.loader.grub.useOSProber = true;
  boot.kernelParams = [
    "console=ttyS0,115200"
    "console=tty0"
    "console=hvc0"
  ];
  boot.loader.grub.extraConfig = ''
  serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1
  terminal_input --append serial
  terminal_output --append serial
'';

  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Paris";

  # Select internationalisation properties.
  i18n.defaultLocale = "fr_FR.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "fr_FR.UTF-8";
    LC_IDENTIFICATION = "fr_FR.UTF-8";
    LC_MEASUREMENT = "fr_FR.UTF-8";
    LC_MONETARY = "fr_FR.UTF-8";
    LC_NAME = "fr_FR.UTF-8";
    LC_NUMERIC = "fr_FR.UTF-8";
    LC_PAPER = "fr_FR.UTF-8";
    LC_TELEPHONE = "fr_FR.UTF-8";
    LC_TIME = "fr_FR.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the KDE Plasma Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;

  # Configure keymap in X11
  services.xserver = {
    layout = "fr";
    xkbVariant = "";
  };

  # Configure console keymap
  console.keyMap = "fr";

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.idiot = {
    isNormalUser = true;
    description = "IdiotUser";
    extraGroups = [ "networkmanager" "wheel" ];
    packages = with pkgs; [
      firefox
      kate
    #  thunderbird
    ];
  };
  users.defaultUserShell = pkgs.zsh;
  # Enable automatic login for the user.
  services.xserver.displayManager.autoLogin.enable = true;
  services.xserver.displayManager.autoLogin.user = "idiot";

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    home-manager
    wget
    curl
    chezmoi
    fzf
    zinit
    zsh-autocomplete
    zsh-completions
    nix-zsh-completions
    zsh-autosuggestions
    exa
    zsh-powerlevel10k
    gitstatus
    zsh-fast-syntax-highlighting
    zsh-history-search-multi-word
    zsh-completions
    delta
    htop
    (neovim.override {
      vimAlias = true;
      configure = {
        packages.myPlugins = with pkgs.vimPlugins; {
          start = [ vim-nix vim-airline vim-airline-themes catppuccin-nvim rainbow nerdtree nerdcommenter nvim-surround vim-oscyank syntastic ctrlp-vim ];
          opt = [];
        };
        customRC = (builtins.readFile /etc/nixos/dotfiles/private_dot_config/nvim/init.vim);
      };
    })
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  programs.zsh = {
    enable = true;
    enableCompletion = false;
    histFile = "$HOME/.histfile";
    promptInit = (builtins.readFile /etc/nixos/dotfiles/dot_zshrc) + (builtins.readFile /etc/nixos/dotfiles/dot_bash_aliases);
  };

  home-manager.useGlobalPkgs = true;
  home-manager.users.root = { pkgs, ... }: {
    home.stateVersion = "23.05";
    programs.zoxide.enable = true;
    programs.home-manager.enable = true;
  };

  # programs.neovim = {
  #   enable = true;
  #   defaultEditor = true;
  #   viAlias = true;
  #   vimAlias = true;
  #   configure = {
  #     customRC = (builtins.readFile /etc/nixos/dotfiles/private_dot_config/nvim/init.vim);
  #     packages.myVimPackage = with pkgs.vimPlugins; {
  #       start = [ vim-nix vim-airline vim-airline-themes catppuccin-nvim rainbow nerdtree nerdcommenter nvim-surround vim-oscyank syntastic ctrlp-vim ];
  #     };
  #   };
  # };

  programs.git = {
    enable = true;
  };

  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    # require public key authentication for better security
    #settings.PasswordAuthentication = false;
    settings.KbdInteractiveAuthentication = false;
    settings.PermitRootLogin = "prohibit-password";
  };
  users.users."idiot".openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOK/j25KWESFh7rA/dl3LuyED4RIORFvOFRjJWDav9vH" "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGu7MjwO02ig6GymhbgjLsapm3eTOQp4hwPgIn/uNbtc"
  ];
  users.users."root".openssh.authorizedKeys.keyFiles = [
    /etc/nixos/authorized_keys
  ];
  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  systemd.targets.sleep.enable = false;
  systemd.targets.suspend.enable = false;
  systemd.targets.hibernate.enable = false;
  systemd.targets.hybrid-sleep.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

}