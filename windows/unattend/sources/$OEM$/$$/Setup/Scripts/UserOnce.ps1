Get-AppxPackage -Name 'Microsoft.Copilot' | Remove-AppxPackage
# Use classic context (right-click) menu in Windows 11
New-Item  -Path 'Registry::HKCU\Software\Classes\CLSID\{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}\InprocServer32' -Force
Set-ItemProperty -Path 'Registry::HKCU\Software\Classes\CLSID\{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}\InprocServer32' -Name '(Default)' -Value '' -Type 'String' -Force;

# Open File Explorer to This PC instead of Quick access
Set-ItemProperty -LiteralPath 'Registry::HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Name 'LaunchTo' -Type 'DWord' -Value 1;

Set-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize" -Name AppsUseLightTheme -Value 0
Set-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize" -Name SystemUsesLightTheme -Value 0
New-Item -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion" -Name Search -Force
Set-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Search" -Name SearchBoxTaskbarMode -Value 0 -Type DWord -Force
((New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() | Where-Object {$_.Path -eq "Microsoft.WindowsStore_8wekyb3d8bbwe!App"}).Verbs() | Where-Object {$_.Name.replace('&','') -match 'Désépingler de la barre des tâches|Unpin from Taskbar'} | ForEach-Object{ $_.DoIt()}
((New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() | Where-Object {$_.Path -eq "MSEdge"}).Verbs() | Where-Object {$_.Name.replace('&','') -match 'Désépingler de la barre des tâches|Unpin from Taskbar'} | ForEach-Object{ $_.DoIt()}

Remove-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Taskband' -Name '*';

Get-Process -Name 'explorer' -ErrorAction 'SilentlyContinue' | Where-Object -FilterScript { $_.SessionId -eq ( Get-Process -Id $PID ).SessionId } | Stop-Process -Force