#!/bin/sh

set -eo

usage()
{
	cat << EOF >&2
Usage: $0 [-a] [-h]
	-a	Build images for AMD64 and ARM64
	-h	Display this help and exit
	-k	Build images only for Kali
	-p	Prune images
EOF
	exit 1
}

echocyan()
{
	printf "%b" "\033[1;36m$*\033[0m\n"
}
echored()
{
	printf "%b" "\033[1;31m$*\033[0m\n"
}
echoyellow()
{
	printf "%b" "\033[0;33m$*\033[0m\n"
}
echomagenta()
{
	printf "%b" "\033[0;35m$*\033[0m\n"
}

while getopts ahk name
do
	case $name in
	a)
		echomagenta "Will build multi-arch images for AMD64 and ARM64."
		MULTIARCH=true
		;;
	k)
		echomagenta "Will build images for Kali only."
		DISTROS="kali"
		;;
	p)
		echomagenta "Pruning images."
		podman image prune -af
		;;
	*)
		usage
		;;
	esac
done

if ! podman login --get-login registry.gitlab.com > /dev/null; then
	echored "Not logged in the registry. Please log in before running this script"
	exit 1
fi

if [ -z "$DISTROS" ]; then
	DISTROS="kali kali-kde kali-xfce debian alma"
	buildah build --ulimit nofile=65535:65535 --tag registry.gitlab.com/keysuke/install-files/arch -f Containerfile-arch .. &&\
	buildah push registry.gitlab.com/keysuke/install-files/arch
	buildah build --ulimit nofile=65535:65535 --tag registry.gitlab.com/keysuke/install-files/blackarch -f Containerfile-blackarch .. &&\
	buildah push registry.gitlab.com/keysuke/install-files/blackarch
fi

for distro in $(echo $DISTROS); do
	if [ "$MULTIARCH" = true ]; then
		manifest=registry.gitlab.com/keysuke/install-files/$distro
		buildah build --ulimit nofile=65535:65535 --jobs=2 --platform=linux/amd64,linux/arm64/v8 --manifest "$manifest" -f Containerfile-$distro ..&&\
		buildah push registry.gitlab.com/keysuke/install-files/$distro
	else
		buildah build --ulimit nofile=65535:65535 --tag registry.gitlab.com/keysuke/install-files/$distro -f Containerfile-$distro .. &&\
		buildah push registry.gitlab.com/keysuke/install-files/$distro
	fi
done



exit 0