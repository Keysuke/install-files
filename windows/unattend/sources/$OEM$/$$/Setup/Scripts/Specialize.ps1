New-Item -Path "HKLM:\SYSTEM\Setup\MoSetup" -Force
New-ItemProperty -Path "HKLM:\SYSTEM\Setup\MoSetup" -Name AllowUpgradesWithUnsupportedTPMOrCPU -Value 1 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\OOBE" -Name BypassNRO -Value 1 -PropertyType DWord -Force
Remove-Item -Path "HKLM:\SOFTWARE\Microsoft\WindowsUpdate\Orchestrator\UScheduler_Oobe\DevHomeUpdate" -Force
Remove-Item -Path "C:\Users\Default\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\OneDrive.lnk" -ErrorAction SilentlyContinue
Remove-Item -Path "C:\Windows\System32\OneDriveSetup.exe"
Remove-Item -Path "C:\Windows\SysWOW64\OneDriveSetup.exe" -ErrorAction SilentlyContinue
Remove-Item -Path "HKLM:\SOFTWARE\Microsoft\WindowsUpdate\Orchestrator\UScheduler_Oobe\OutlookUpdate" -Force
New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Communications" -Name ConfigureChatAutoInstall -Value 0 -PropertyType DWord -Force
Get-AppxProvisionedPackage -Online | Where-Object DisplayName -In (Get-Content $env:windir\Setup\Scripts\RemovePackages.txt ) | Remove-AppxProvisionedPackage -AllUsers -Online *>&1 >> $env:windir\Setup\Scripts\RemovePackages.log
Get-WindowsCapability -Online | Where-Object {($_.Name -split '~')[0] -in (Get-Content $env:windir\Setup\Scripts\RemoveCapabilities.txt ) } | Remove-WindowsCapability -Online *>&1 >> $env:windir\Setup\Scripts\RemoveCapabilities.log
Get-WindowsOptionalFeature -Online | Where-Object -Property 'State' -NotIn -Value @('Disabled';'DisabledWithPayloadRemoved';) | Where-Object FeatureName -In (Get-Content $env:windir\Setup\Scripts\RemoveFeatures.txt ) | Disable-WindowsOptionalFeature -Online -Remove -NoRestart -ErrorAction 'Continue' *>&1 >> $env:windir\Setup\Scripts\RemoveFeatures.log

Copy-Item -Path "$env:windir\Setup\Scripts\LayoutModification.xml" -Destination "C:\Users\Default\AppData\Local\Microsoft\Windows\Shell\LayoutModification.xml"
New-Item -Path "HKLM:\SOFTWARE\Microsoft\PolicyManager\current\device\Start" -Force
New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\PolicyManager\current\device\Start" -Name ConfigureStartPins -Value '{ "pinnedList": [] }' -PropertyType String -Force
New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\PolicyManager\current\device\Start" -Name ConfigureStartPins_ProviderSet -Value 1 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\PolicyManager\current\device\Start" -Name ConfigureStartPins_WinningProvider -Value 'B5292708-1619-419B-9923-E5D9F3925E71' -PropertyType String -Force
New-Item -Path "HKLM:\SOFTWARE\Microsoft\PolicyManager\providers\B5292708-1619-419B-9923-E5D9F3925E71\default\Device\Start" -Force
New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\PolicyManager\providers\B5292708-1619-419B-9923-E5D9F3925E71\default\Device\Start" -Name ConfigureStartPins -Value '{ "pinnedList": [] }' -PropertyType String -Force
New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\PolicyManager\providers\B5292708-1619-419B-9923-E5D9F3925E71\default\Device\Start" -Name ConfigureStartPins_LastWrite -Value 1 -PropertyType DWord -Force
icacls.exe C:\ /remove:g "*S-1-5-11"
fsutil.exe behavior set disableLastAccess 1
New-Item -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Force
New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name AUOptions -Value 4 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name NoAutoRebootWithLoggedOnUsers -Value 1 -PropertyType DWord -Force

reg.exe load "HKLM\DefaultUser" "C:\Users\Default\NTUSER.DAT"
New-Item -Path "HKLM:\DefaultUser\Software\Policies\Microsoft\Windows\WindowsCopilot" -Force
New-ItemProperty -Path "HKLM:\DefaultUser\Software\Policies\Microsoft\Windows\WindowsCopilot" -Name TurnOffWindowsCopilot -Value 1 -PropertyType DWord -Force
Remove-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\Run" -Name OneDriveSetup -Force
New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name ContentDeliveryAllowed -Value 0 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name FeatureManagementEnabled -Value 0 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name OEMPreInstalledAppsEnabled -Value 0 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name PreInstalledAppsEnabled -Value 0 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name PreInstalledAppsEverEnabled -Value 0 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name SilentInstalledAppsEnabled -Value 0 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name SoftLandingEnabled -Value 0 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name SubscribedContentEnabled -Value 0 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name SubscribedContent-310093Enabled -Value 0 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name SubscribedContent-338387Enabled -Value 0 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name SubscribedContent-338388Enabled -Value 0 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name SubscribedContent-338389Enabled -Value 0 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name SubscribedContent-338393Enabled -Value 0 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name SubscribedContent-353698Enabled -Value 0 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name SystemPaneSuggestionsEnabled -Value 0 -PropertyType DWord -Force
New-Item -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion" -Name Search
Set-ItemProperty -Path "HKLM:\DefaultUser\SOFTWARE\Microsoft\Windows\CurrentVersion\Search" -Name SearchBoxTaskbarMode -Value 0 -Type DWord -Force
Set-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name TaskbarGlomLevel -Value 1
Set-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name HideFileExt -Value 0
Set-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name ShowCortanaButton -Value 0
Set-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name ShowTaskViewButton -Value 0
Set-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name TaskbarDa -Value 0 -ErrorAction SilentlyContinue
Set-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name TaskbarAl -Value 0
Set-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name StartShownOnUpgrade -Value 1
New-Item -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\RunOnce" -Force
Set-ItemProperty "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\RunOnce" -Name '!UserFirstRun' -Value 'powershell.exe -NoProfile -ExecutionPolicy Bypass -WindowStyle Hidden -File "C:\Windows\Setup\Scripts\UserOnce.ps1"'

New-Item -Path "HKLM:\DefaultUser\SOFTWARE\Microsoft\Windows\CurrentVersion\GameDVR" -Force
New-ItemProperty -Path "HKLM:\DefaultUser\SOFTWARE\Microsoft\Windows\CurrentVersion\GameDVR" -Force -Name "AppCaptureEnabled" -Value "0" -PropertyType DWord
New-ItemProperty -Path "HKLM:\DefaultUser\SOFTWARE\Microsoft\Windows\CurrentVersion\GameDVR" -Force -Name "NoWinKeys" -Value "1" -PropertyType DWord
New-Item -Path "HKLM:\DefaultUser\System\GameConfigStore" -Force
New-ItemProperty -Path "HKLM:\DefaultUser\System\GameConfigStore" -Force -Name "GameDVR_Enabled" -Value "0" -PropertyType DWord
New-ItemProperty -Path "HKLM:\DefaultUser\System\GameConfigStore" -Force -Name "GameDVR_FSEBehaviorMode" -Value "2" -PropertyType DWord

if ((Get-ItemPropertyValue -Path "HKLM:\SYSTEM\CurrentControlSet\Services\WinDefend" -Name "Start") -eq 4)
{
	Write-Host "Defender is permanently disabled. Disabling other security features and disabling Windows Update." -ForegroundColor "Magenta"

	Set-ExecutionPolicy -Scope 'LocalMachine' -ExecutionPolicy 'Bypass' -Force
	net.exe accounts /maxpwage:UNLIMITED

	New-Item -Path "HKLM:\DefaultUser\Software\Microsoft\Edge" -Force
	New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Edge" -Name SmartScreenEnabled -Value 0 -PropertyType DWord -Force
	New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Edge" -Name SmartScreenPuaEnabled -Value 0 -PropertyType DWord -Force
	New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\AppHost" -Name EnableWebContentEvaluation -Value 0 -PropertyType DWord -Force
	New-ItemProperty -Path "HKLM:\DefaultUser\Software\Microsoft\Windows\CurrentVersion\AppHost" -Name PreventOverride -Value 0 -PropertyType DWord -Force

	New-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Control\CI\Policy" -Name VerifiedAndReputablePolicyState -Value 0 -PropertyType DWord -Force
	New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" -Name SmartScreenEnabled -Value "Off" -PropertyType String -Force
	New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WTDS\Components" -Name ServiceEnabled -Value 0 -PropertyType DWord -Force
	New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WTDS\Components" -Name NotifyMalicious -Value 0 -PropertyType DWord -Force
	New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WTDS\Components" -Name NotifyPasswordReuse -Value 0 -PropertyType DWord -Force
	New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WTDS\Components" -Name NotifyUnsafeApp -Value 0 -PropertyType DWord -Force
	New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name EnableLUA -Value 0 -PropertyType DWord -Force

	Register-ScheduledTask -TaskName 'PauseWindowsUpdate' -Xml $( Get-Content -LiteralPath "$env:windir\Setup\Scripts\PauseWindowsUpdate.xml" -Raw )
}
else
{
	Write-Host "Defender is enabled." -ForegroundColor "Magenta"
}

reg.exe unload "HKLM\DefaultUser"

foreach ($RegPath in ("Registry::HKEY_CLASSES_ROOT\ms-gamebar","Registry::HKEY_CLASSES_ROOT\ms-gamebarservices"))
{
	New-Item -Path $RegPath -Force -Value "URL:ms-gamebar"
	New-ItemProperty -Path $RegPath -Force -Name "URL Protocol" -Value "" -PropertyType String
	New-ItemProperty -Path $RegPath -Force -Name "NoOpenWith" -Value "" -PropertyType String
	New-Item -Path "$RegPath\shell\open\command" -Force -Value "\`"$env:SystemRoot\System32\systray.exe\`""
}

New-Item -Path "HKLM:\Software\Policies\Microsoft\Windows\CloudContent" -Force
New-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\CloudContent" -Name DisableWindowsConsumerFeatures -Value 0 -PropertyType DWord -Force

Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager\Power" -Name HiberbootEnabled -Value 0
Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Control\Power" -Name HibernateEnabled -Value 1
Set-ItemProperty -Path "Registry::HKEY_USERS\.DEFAULT\Control Panel\Keyboard" -Name InitialKeyboardIndicators -Value 2
Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Control\Keyboard Layouts\00000804" -Name "Layout File" -Value "KBDFR.DLL"
Set-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name Start_SearchFiles -Value 1
Set-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name ShowTaskViewButton -Value 0
Set-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" -Name HideSCAMeetNow -Value 1
New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" -Name DisableEdgeDesktopShortcutCreation -PropertyType DWord -Value 1 -Force
New-Item -Path "HKLM:\Software\Policies\Microsoft\Edge" -Force
Set-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Edge" -Name HideFirstRunExperience -Value 1
New-Item -Path "HKLM:\Software\Policies\Microsoft\Windows\Explorer" -Force
Set-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\Explorer" -Name DisableSearchBoxSuggestions -Value 1 -Force
Set-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\Explorer" -Name HideRecommendedSection -Value 1 -Force
New-Item -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows" -Name "Windows Search" -Force
New-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search' -Name AllowCortana -PropertyType DWord -Value 0 -Force

Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\tzautoupdate" -Name Start -Value 3

New-Item -Path "HKLM:\SOFTWARE\Policies\Microsoft\Dsh" -Force
New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Dsh" -Name AllowNewsAndInterests -Value 0 -PropertyType DWord -Force
New-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Control\BitLocker" -Name PreventDeviceEncryption -Value 1 -PropertyType DWord -Force


if( [System.Environment]::OSVersion.Version.Build -ge 20000 )
{
	$json = '{"pinnedList":[]}'
	$key = 'Registry::HKLM\SOFTWARE\Microsoft\PolicyManager\current\device\Start'
	New-Item -Path $key -ItemType 'Directory' -Force
	Set-ItemProperty -LiteralPath $key -Name 'ConfigureStartPins' -Value $json -Type 'String'
}

Disable-ComputerRestore -Drive 'C:\'
Remove-Item "C:\Users\Public\Desktop\Microsoft Edge.lnk","C:\Windows.old" -Force