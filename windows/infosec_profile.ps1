if ($PSVersionTable.PSEdition -eq "Core")
{
	Write-Host "Importing the infosec modules in pwsh.exe is not supported. Please use powershell.exe." -ForegroundColor "Magenta"
}
elseif (-Not(Get-Service -Name cexecsvc -ErrorAction SilentlyContinue) -And (Get-MpComputerStatus -ErrorAction SilentlyContinue | Select-Object -ExpandProperty RealTimeProtectionEnabled) -eq $true)
{
	Write-Host "Defender is enabled. Not importing the InfoSec modules" -ForegroundColor "Magenta"
}
elseif ((Write-Output Unrestricted RemoteSigned Bypass) -contains ($ExecutionPolicy = Get-ExecutionPolicy))
{
	Import-Module "C:\Tools\PowerSploit\PowerSploit.psm1"
	Get-ChildItem "C:\Tools" -Filter *.ps1 | Foreach-Object { Import-Module $_.FullName }
	Write-Host "Imported InfoSec modules." -ForegroundColor "Cyan"
}
else
{
	Write-Host "PowerShell's execution policy $ExecutionPolicy is preventing the importation of InfoSec modules." -ForegroundColor "Magenta"
}