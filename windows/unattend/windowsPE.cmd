@echo off

powercfg /s 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c

reg.exe add "HKLM\SYSTEM\Setup\LabConfig" /v BypassTPMCheck /t REG_DWORD /d 1 /f > NUL
reg.exe add "HKLM\SYSTEM\Setup\LabConfig" /v BypassSecureBootCheck /t REG_DWORD /d 1 /f > NUL
reg.exe add "HKLM\SYSTEM\Setup\LabConfig" /v BypassStorageCheck /t REG_DWORD /d 1 /f > NUL
reg.exe add "HKLM\SYSTEM\Setup\LabConfig" /v BypassCPUCheck /t REG_DWORD /d 1 /f > NUL
reg.exe add "HKLM\SYSTEM\Setup\LabConfig" /v BypassRAMCheck /t REG_DWORD /d 1 /f > NUL
reg.exe add "HKLM\SYSTEM\Setup\LabConfig" /v BypassDiskCheck /t REG_DWORD /d 1 /f > NUL

if %PROCESSOR_ARCHITECTURE%==AMD64 (
	set ARCH=x64
) else (
	set ARCH=x86
)

wmic os get ProductType | find /i "1" > NUL && set PRODUCTTYPE=workstation

if %PRODUCTTYPE%==workstation (
	wmic os get version | find /i "6.0" > NUL && set WINVER=w7
	wmic os get version | find /i "6.1" > NUL && set WINVER=w7
	wmic os get version | find /i "6.2" > NUL && set WINVER=w8
	wmic os get version | find /i "6.3" > NUL && set WINVER=w8.1
	wmic os get version | find /i "10.0" > NUL && set WINVER=w10
	wmic os get version | find /i "10.0.22" > NUL && set WINVER=w11
	wmic os get version | find /i "10.0.26" > NUL && set WINVER=w11
) else (
	wmic os get version | find /i "14393" > NUL && set WINVER=2k16
	wmic os get version | find /i "17763" > NUL && set WINVER=2k19
	wmic os get version | find /i "20348" > NUL && set WINVER=2k22
	wmic os get version | find /i "26100" > NUL && set WINVER=2k22
)

wpeinit

for %%A in (D E F G H I J K L M N O P Q R S T U V W X Y Z) do if exist %%A:\boot set DRIVE=%%A
@REM drvload %DRIVE%:\$WinPEDriver$\viostor\%WINVER%\%PROCESSOR_ARCHITECTURE%\viostor.inf
@REM drvload %DRIVE%:\$WinPEDriver$\NetKVM\%WINVER%\%PROCESSOR_ARCHITECTURE%\netkvm.inf
wpeutil UpdateBootInfo
for /f "tokens=2* delims=	 " %%A in ('reg query HKLM\System\CurrentControlSet\Control /v PEFIRMWAREType') do set FIRMWARE=%%B


if %FIRMWARE%==0x1 (
	echo MBR boot detected.
	set FORMATFILEDIST=%DRIVE%:\format-MBR.txt.dist
	goto Format
) else (
	echo UEFI boot detected.
	set FORMATFILEDIST=%DRIVE%:\format-UEFI.txt.dist
	goto Format
)

:Format
set FORMATFILE=X:\format.txt
echo.
echo Listing physical drives.
wmic diskdrive get index,interfacetype,mediatype,model,size
setlocal
:PROMPT
set /p "disktoformat=Enter the index of the disk you want to format (press Q to skip this step): "
@REM set disktoformat=Q
IF /I "%disktoformat%" == "Q" echo Skipping automatic partitionning.&& ping -n 3 127.0.0.1 > nul && GOTO PROMPTDEF
echo.
SET /P FORMAT=" WARNING, THIS WILL DELETE YOUR DATA ON DISK %disktoformat%, MAKE SURE IT IS THE RIGHT DISK! Do you want to automatically format disk number %disktoformat%? (Y/N) "

IF /I "%FORMAT%" NEQ "Y" GOTO PROMPT
echo select disk %disktoformat% > %FORMATFILE%
more +1 %FORMATFILEDIST% >> %FORMATFILE%
diskpart /s %FORMATFILE% && echo. && echo Successfully partitioned disk number %disktoformat%. && ping -n 6 127.0.0.1 > nul && GOTO PROMPTDEF || echo Error while trying to partition. Trying again. && GOTO FORMAT

:PROMPTDEF
set /p "disabledefender=Do you want to disable Defender on this install? (Y/N): "
IF /I "%disabledefender%" == "N" echo Defender will be enabled on this install.&& ping -n 6 127.0.0.1 > nul && GOTO END
IF /I "%disabledefender%" == "Y" echo Defender will be DISABLED on this install.&& start cscript.exe //E:vbscript %DRIVE%:\DisableDefender.vbs

:END
endlocal
exit
