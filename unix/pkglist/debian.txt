zsh zsh-autosuggestions neovim git tree wget rsync curl openssl xclip plocate
fzf zoxide grc
tmux mosh multitail tmate bat ncdu duf lsof htop btop entr hexyl nnn yank logrotate
ngrep nethogs autossh tcpdump whois dhcping corkscrew openresolv elinks jq miller sl cowsay
fd-find ripgrep asciinema jc
hyperfine tldr-py
strace sshuttle traceroute bc rlwrap
hping3 mtr iproute2 net-tools bind9-dnsutils rinetd ncat iptables-persistent iputils-ping
zstd unzip unrar 7zip zip secure-delete

numlockx less acl file libxkbcommon-x11-0
locales dialog aptitude apt-utils dpkg-dev kbd console-setup fontconfig command-not-found unattended-upgrades procps kmod
openssh-server
# linux-headers-amd64 firmware-linux
# fonts-powerline fonts-inconsolata fonts-lato fonts-noto unifont
# firefox-esr-l10n-fr webext-ublock-origin-firefox
# cherrytree remmina filezilla vlc timeshift
# KDE ark okular-extra-backends sddm-theme-debian-breeze plasma-workspace-wayland qtwayland5 kde-config-fcitx fcitx-libpinyin fcitx-frontend-all
# GNOME # network-manager-openvpn-gnome
# KVM guest spice-vdagent
# KVM host virt-manager ovmf
# Steam steam steam-devices libgl1-mesa-dri libglx-mesa0 mesa-vulkan-drivers libglx-mesa0:i386 mesa-vulkan-drivers:i386 libgl1-mesa-dri:i386