$timeout=10
Write-Host "Press a key before $timeout seconds to enable remote access on this Windows installation." -ForegroundColor "Magenta"
$stopwatch = [System.Diagnostics.Stopwatch]::StartNew()
while (-not [Console]::KeyAvailable)
{
	if ($stopwatch.ElapsedMilliseconds -gt $timeout*1000 )
	{
		Write-Host "Continuing without remote access." -ForegroundColor "Cyan"
		exit
	}
	Start-Sleep -Seconds 1
	$countdown=$timeout - [Math]::Round(($stopwatch.ElapsedMilliseconds/1000),0)
	if ( $countdown -gt 0 )
	{
		Write-Host "$countdown seconds left."
	}
}
$stopwatch.Stop()

Write-Output $null >> "$env:SystemDrive\.remoteaccess"
Write-Host "Remote access will be configured for this Windows installation." -ForegroundColor "Magenta"

Start-Sleep -Seconds 10