if((Get-Service -Name cexecsvc -ErrorAction SilentlyContinue))
{
	Write-Host "Running in a container." -ForegroundColor "Cyan"
	$IsContainer = $True
}

if (-Not($IsContainer) -And (Get-MpComputerStatus | Select-Object -ExpandProperty "IsTamperProtected") -eq $true)
{
	Write-Host "Defender is enabled. Please disable it before running this script. " -ForegroundColor "Red"
	Exit 1
}
# Based on https://raw.githubusercontent.com/ZeroPointSecurity/RTOVMSetup/master/win10.choco
Write-Host "Will install information security tools." -ForegroundColor "Cyan"

# Enable access to Public SMB shares <https://docs.microsoft.com/en-us/troubleshoot/windows-server/networking/guest-access-in-smb2-is-disabled-by-default>
Set-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\Services\LanmanWorkstation\Parameters -Name "AllowInsecureGuestAuth" -Value 1 -type DWord

# Uncomment the following line to install Visual Studio and its dependencies in another drive.
# $NewProgramFilesDrive = "D:"

Add-Type -Assembly System.IO.Compression.FileSystem

New-Item -Path $env:SystemDrive\ -Name Temp -ItemType Directory -ErrorAction SilentlyContinue
New-Item -Path $env:SystemDrive\ -Name Payloads -ItemType Directory -ErrorAction SilentlyContinue

$env:TEMP = "$env:SystemDrive\Temp"
$env:TMP = "$env:SystemDrive\Temp"
$env:TOOLS = "$env:SystemDrive\Tools"

if (Test-Path "$env:TOOLS" -PathType Container)
{
	Write-Host "Directory $env:TOOLS already exists. Deleting it..." -ForegroundColor "Cyan"
	Start-Sleep -Seconds 10
	Remove-Item -Recurse -Force "$env:TOOLS"
}

New-Item -Path $env:SystemDrive\ -Name Tools -ItemType Directory -ErrorAction SilentlyContinue
$ACL = Get-Acl -Path "$env:TOOLS"
$User = New-Object System.Security.Principal.Ntaccount([System.Security.Principal.WindowsIdentity]::GetCurrent().Name)
$ACL.SetOwner($User)
$permission = New-Object System.Security.AccessControl.FileSystemAccessRule([System.Security.Principal.WindowsIdentity]::GetCurrent().Name, "FullControl", "ContainerInherit,ObjectInherit", "None", "Allow")
$ACL.SetAccessRule($permission)
$ACL | Set-Acl -Path "$env:TOOLS"

$env:TOOLS_SRC = "$env:TOOLS\src"
$env:TOOLS_EXE = "$env:TOOLS\exe"

$VS_Params = "--wait --passive --add Microsoft.VisualStudio.Workload.ManagedDesktop Microsoft.VisualStudio.Workload.NativeDesktop Microsoft.VisualStudio.ComponentGroup.VC.Tools.142.x86.x64 Microsoft.VisualStudio.Component.Windows10SDK.18362 Microsoft.VisualStudio.Component.IntelliCode Component.Microsoft.VisualStudio.LiveShare.2022"

New-Item -Path $env:TOOLS\ -Name src -ItemType Directory -ErrorAction SilentlyContinue
New-Item -Path $env:TOOLS\ -Name exe -ItemType Directory -ErrorAction SilentlyContinue
New-Item -Path $env:TOOLS\ -Name old -ItemType Directory -ErrorAction SilentlyContinue

# GitHub dependencies
$WebClient = (New-Object System.Net.WebClient)
Write-Host "Installing the dependencies from GitHub" -ForegroundColor "Cyan"

git clone -q https://github.com/ohpe/juicy-potato.git $env:TOOLS_SRC\juicy-potato
git clone -q https://github.com/djhohnstein/SharpChromium.git $env:TOOLS_SRC\SharpChrome
git clone -q https://github.com/FortyNorthSecurity/Egress-Assess.git $env:TOOLS_SRC\Egress-Assess
git clone -q https://github.com/FSecureLABS/SharpGPOAbuse.git $env:TOOLS_SRC\SharpGPOAbuse
git clone -q https://github.com/gentilkiwi/mimikatz.git $env:TOOLS_SRC\mimikatz
git clone -q https://github.com/GhostPack/Seatbelt.git $env:TOOLS_SRC\Seatbelt
git clone -q https://github.com/GhostPack/Rubeus $env:TOOLS_SRC\Rubeus
git clone -q https://github.com/hfiref0x/UACME.git $env:TOOLS_SRC\UACME
git clone -q https://github.com/leechristensen/SpoolSample.git $env:TOOLS_SRC\SpoolSample
git clone -q https://github.com/p3nt4/PowerShdll.git $env:TOOLS_SRC\PowerShdll
git clone -q https://github.com/rasta-mouse/MiscTools.git $env:TOOLS_SRC\MiscTools
git clone -q https://github.com/rasta-mouse/Watson.git $env:TOOLS_SRC\Watson
git clone -q https://github.com/ZeroPointSecurity/PhishingTemplates.git $env:TOOLS_SRC\PhishingTemplates
git clone -q https://github.com/WithSecureLabs/physmem2profit.git $env:TOOLS_SRC\physmem2profit
git clone -q https://github.com/tyranid/DotNetToJScript.git $env:TOOLS_SRC\DotNetToJScript
git clone -q https://github.com/outflanknl/EvilClippy.git $env:TOOLS_SRC\EvilClippy
git clone -q https://github.com/0x09AL/RdpThief.git $env:TOOLS_SRC\RdpThief
git clone -q https://github.com/DiedB/SharpRDP.git $env:TOOLS_SRC\SharpRDP
git clone -q https://github.com/rasta-mouse/ThreatCheck.git $env:TOOLS_SRC\ThreatCheck

$Path = [System.Environment]::GetEnvironmentVariable("PATH", "Machine"), $env:TOOLS_EXE -Join [IO.Path]::PathSeparator
[System.Environment]::SetEnvironmentVariable( "Path", $Path, "Machine" )


git clone -q https://github.com/PowerShellMafia/PowerSploit.git $env:TOOLS\PowerSploit

$WebClient.DownloadFile("https://raw.githubusercontent.com/NetSPI/PowerUpSQL/master/PowerUpSQL.ps1", "$env:TOOLS\PowerUpSQL.ps1")
$WebClient.DownloadFile("https://raw.githubusercontent.com/BloodHoundAD/BloodHound/master/Collectors/SharpHound.ps1", "$env:TOOLS\old\SharpHound1.ps1")
$WebClient.DownloadFile("https://raw.githubusercontent.com/dafthack/MailSniper/master/MailSniper.ps1", "$env:TOOLS\MailSniper.ps1")
$WebClient.DownloadFile("https://raw.githubusercontent.com/HarmJ0y/DAMP/master/Add-RemoteRegBackdoor.ps1", "$env:TOOLS\Add-RemoteRegBackdoor.ps1")
$WebClient.DownloadFile("https://raw.githubusercontent.com/HarmJ0y/DAMP/master/RemoteHashRetrieval.ps1", "$env:TOOLS\RemoteHashRetrieval.ps1")
$WebClient.DownloadFile("https://raw.githubusercontent.com/rasta-mouse/Sherlock/master/Sherlock.ps1", "$env:TOOLS\Sherlock.ps1")
$WebClient.DownloadFile("https://raw.githubusercontent.com/itm4n/PrivescCheck/refs/heads/master/src/exploit/PointAndPrint.ps1", "$env:TOOLS\PointAndPrint.ps1")
$WebClient.DownloadFile("https://raw.githubusercontent.com/itm4n/PrivescCheck/master/PrivescCheck.ps1", "$env:TOOLS\PrivescCheck.ps1")
$WebClient.DownloadFile("https://raw.githubusercontent.com/dafthack/HostRecon/master/HostRecon.ps1", "$env:TOOLS\HostRecon.ps1")
$WebClient.DownloadFile("https://raw.githubusercontent.com/Kevin-Robertson/Powermad/master/Powermad.ps1", "$env:TOOLS\Powermad.ps1")
$WebClient.DownloadFile("https://raw.githubusercontent.com/Kevin-Robertson/Powermad/master/Invoke-DNSUpdate.ps1", "$env:TOOLS\Invoke-DNSUpdate.ps1")
$WebClient.DownloadFile("https://raw.githubusercontent.com/leoloobeek/LAPSToolkit/master/LAPSToolkit.ps1", "$env:TOOLS\LAPSToolkit.ps1")
$WebClient.DownloadFile("https://raw.githubusercontent.com/F4l13n5n0w/PowerSharpPack/master/PowerSharpPack.ps1", "$env:TOOLS\PowerSharpPack.ps1")
$WebClient.DownloadFile("https://raw.githubusercontent.com/wavestone-cdt/Invoke-CleverSpray/master/Invoke-CleverSpray.ps1", "$env:TOOLS\Invoke-CleverSpray.ps1")

$WebClient.DownloadFile("https://github.com/BloodHoundAD/BloodHound/raw/master/Collectors/SharpHound.exe", "$env:TOOLS_EXE\SharpHound1.exe")
$WebClient.DownloadFile("https://github.com/tevora-threat/SharpView/raw/master/Compiled/SharpView.exe", "$env:TOOLS_EXE\SharpView.exe")
$WebClient.DownloadFile("https://github.com/carlospolop/PEASS-ng/releases/latest/download/winPEASany_ofs.exe", "$env:TOOLS_EXE\winPEAS.exe")
$WebClient.DownloadFile("https://github.com/carlospolop/PEASS-ng/releases/latest/download/winPEAS.bat", "$env:TOOLS_EXE\winPEAS.bat")
$WebClient.DownloadFile("https://github.com/SnaffCon/Snaffler/releases/latest/download/Snaffler.exe", "$env:TOOLS_EXE\Snaffler.exe")
$WebClient.DownloadFile("https://github.com/Mr-Un1k0d3r/SCShell/raw/master/SCShell.exe", "$env:TOOLS_EXE\SCShell.exe")
$WebClient.DownloadFile("https://github.com/sevagas/macro_pack/releases/latest/download/macro_pack.exe", "$env:TOOLS_EXE\macro_pack.exe")
$WebClient.DownloadFile("https://github.com/shadow1ng/fscan/releases/latest/download/fscan.exe", "$env:TOOLS_EXE\fscan.exe")
$WebClient.DownloadFile("https://github.com/BeichenDream/GodPotato/releases/latest/download/GodPotato-NET4.exe", "$env:TOOLS_EXE\GodPotato.exe")
$WebClient.DownloadFile("https://github.com/gatariee/gocheck/releases/latest/download/gocheck64.exe", "$env:TOOLS_EXE\gocheck.exe")

$WebClient.DownloadFile(((Invoke-RestMethod -Method GET -Uri "https://api.github.com/repos/lkarlslund/Adalanche/releases")[0].assets | Where-Object Name -like "adalanche-collector-windows-x64-*.exe" ).browser_download_url , "$env:TOOLS_EXE\adalanche-collector.exe")

$WebClient.DownloadFile(((Invoke-RestMethod -Method GET -Uri "https://api.github.com/repos/AlessandroZ/LaZagne/releases")[0].assets | Where-Object Name -like "*.exe" ).browser_download_url , "$env:TOOLS_EXE\lazagne.exe")

$WebClient.DownloadFile("https://github.com/gentilkiwi/mimikatz/releases/latest/download/mimikatz_trunk.zip", "$env:TEMP\mimikatz_trunk.zip")
$zipFile = [IO.Compression.ZipFile]::OpenRead("$env:TEMP\mimikatz_trunk.zip")
$zipFile.Entries | Where-Object {$_.FullName -eq 'x64/mimikatz.exe'} | ForEach-Object{[System.IO.Compression.ZipFileExtensions]::ExtractToFile($_, "$env:TOOLS_EXE\mimikatz.exe", $true)}
$zipFile.Entries | Where-Object {$_.FullName -eq 'x64/mimidrv.sys'} | ForEach-Object{[System.IO.Compression.ZipFileExtensions]::ExtractToFile($_, "$env:TOOLS_EXE\mimidrv.sys", $true)}
$zipFile.Entries | Where-Object {$_.FullName -eq 'Win32/mimikatz.exe'} | ForEach-Object{[System.IO.Compression.ZipFileExtensions]::ExtractToFile($_, "$env:TOOLS_EXE\mimikatz32.exe", $true)}
$zipFile.Dispose()

$WebClient.DownloadFile(((Invoke-RestMethod -Method GET -Uri "https://api.github.com/repos/antonioCoco/RunasCs/releases")[0].assets | Where-Object Name -like "*.zip" ).browser_download_url, "$env:TEMP\RunasCs.zip")
$zipFile = [IO.Compression.ZipFile]::OpenRead("$env:TEMP\RunasCs.zip")
$zipFile.Entries | Where-Object {$_.Name -eq 'RunasCs.exe'} | ForEach-Object{[System.IO.Compression.ZipFileExtensions]::ExtractToFile($_, "$env:TOOLS_EXE\RunasCs.exe", $true)}
$zipFile.Entries | Where-Object {$_.Name -eq 'RunasCs_net2.exe'} | ForEach-Object{[System.IO.Compression.ZipFileExtensions]::ExtractToFile($_, "$env:TOOLS_EXE\RunasCs_net2.exe", $true)}
$zipFile.Dispose()

$WebClient.DownloadFile(((Invoke-RestMethod -Method GET -Uri "https://api.github.com/repos/FuzzySecurity/StandIn/releases")[0].assets).browser_download_url, "$env:TEMP\StandIn.zip")
$zipFile = [IO.Compression.ZipFile]::OpenRead("$env:TEMP\StandIn.zip")
$zipFile.Entries | Where-Object {$_.Name -like '*45.exe'} | ForEach-Object{[System.IO.Compression.ZipFileExtensions]::ExtractToFile($_, "$env:TOOLS_EXE\StandIn.exe", $true)}
$zipFile.Entries | Where-Object {$_.Name -like '*35.exe'} | ForEach-Object{[System.IO.Compression.ZipFileExtensions]::ExtractToFile($_, "$env:TOOLS_EXE\StandIn35.exe", $true)}
$zipFile.Dispose()

$WebClient.DownloadFile(((Invoke-RestMethod -Method GET -Uri "https://api.github.com/repos/vletoux/pingcastle/releases")[0].assets).browser_download_url, "$env:TEMP\PingCastle.zip")
Expand-Archive -Force -Path "$env:TEMP\PingCastle.zip" -DestinationPath "$env:TOOLS_EXE\PingCastle"
New-Item -Force -ItemType SymbolicLink -Path "$env:TOOLS_EXE" -Name PingCastle.exe -Target "$env:TOOLS_EXE\PingCastle\PingCastle.exe"

$WebClient.DownloadFile(((Invoke-RestMethod -Method GET -Uri "https://api.github.com/repos/Kevin-Robertson/Inveigh/releases")[0].assets | Where-Object Name -like "Inveigh-*-win-x64-nativeaot-*" ).browser_download_url, "$env:TEMP\Inveigh.zip")
Expand-Archive -Force -Path "$env:TEMP\Inveigh.zip" -DestinationPath "$env:TEMP\Inveigh"
Move-Item -Path "$env:TEMP\Inveigh\Inveigh.exe" -Destination "$env:TOOLS_EXE\Inveigh.exe"

$WebClient.DownloadFile(((Invoke-RestMethod -Method GET -Uri "https://api.github.com/repos/BloodHoundAD/SharpHound/releases")[0].assets | Where-Object Name -NotMatch "debug" ).browser_download_url, "$env:TEMP\SharpHound.zip")
$zipFile = [IO.Compression.ZipFile]::OpenRead("$env:TEMP\SharpHound.zip")
$zipFile.Entries | Where-Object {$_.Name -eq 'SharpHound.exe'} | ForEach-Object{[System.IO.Compression.ZipFileExtensions]::ExtractToFile($_, "$env:TOOLS_EXE\SharpHound.exe", $true)}
$zipFile.Entries | Where-Object {$_.Name -eq 'SharpHound.ps1'} | ForEach-Object{[System.IO.Compression.ZipFileExtensions]::ExtractToFile($_, "$env:TOOLS\SharpHound.ps1", $true)}
$zipFile.Dispose()

$WebClient.DownloadFile(((Invoke-RestMethod -Method GET -Uri "https://api.github.com/repos/OJ/gobuster/releases")[0].assets | Where-Object Name -like "*Windows_x86_64*" ).browser_download_url, "$env:TEMP\gobuster.zip")
$zipFile = [IO.Compression.ZipFile]::OpenRead("$env:TEMP\gobuster.zip")
$zipFile.Entries | Where-Object {$_.Name -eq 'gobuster.exe'} | ForEach-Object{[System.IO.Compression.ZipFileExtensions]::ExtractToFile($_, "$env:TOOLS_EXE\gobuster.exe", $true)}
$zipFile.Dispose()

$WebClient.DownloadFile(((Invoke-RestMethod -Method GET -Uri "https://api.github.com/repos/TheWover/donut/releases")[0].assets | Where-Object Name -like "*.zip").browser_download_url, "$env:TEMP\donut.zip")
$zipFile = [IO.Compression.ZipFile]::OpenRead("$env:TEMP\donut.zip")
$zipFile.Entries | Where-Object {$_.Name -eq 'donut.exe'} | ForEach-Object{[System.IO.Compression.ZipFileExtensions]::ExtractToFile($_, "$env:TOOLS_EXE\donut.exe", $true)}
$zipFile.Dispose()

$WebClient.DownloadFile(((Invoke-RestMethod -Method GET -Uri "https://api.github.com/repos/optiv/ScareCrow/releases")[0].assets | Where-Object Name -like "ScareCrow_*_linux_amd64" ).browser_download_url, "$env:TOOLS_EXE\ScareCrow.exe")

git clone -q https://github.com/Flangvik/SharpCollection.git $env:TOOLS_EXE\SharpCollection
Get-ChildItem "$env:TOOLS_EXE\SharpCollection\NetFramework_4.7_x64" -Filter *.exe | Foreach-Object {
	if (-Not(Test-Path "$env:TOOLS_EXE\$_" -PathType leaf ))
	{
		New-Item -ItemType SymbolicLink -Path "$env:TOOLS_EXE" -Name $_.Name -Target $_.FullName
	}
}

if($IsContainer)
{
	Write-Host "Running in a container. Skipping Defender modifications and Visual Studio installation" -ForegroundColor "Cyan"
	choco install -y python upx
}
else
{
	if ((Get-MpComputerStatus).AMRunningMode -ne "Not running")
	{
		Add-MpPreference -ExclusionPath "$env:SystemDrive\Payloads"
		Add-MpPreference -ExclusionPath $env:TOOLS
		Add-MpPreference -ExclusionPath Z:\
		Set-MpPreference -MAPSReporting Disabled
		Set-MpPreference -SubmitSamplesConsent NeverSend
	}
	if ($NewProgramFilesDrive)
	{
		$NewProgramFiles = "$NewProgramFilesDrive\Program Files (x86)"
		Write-Host "Will install Visual Studio in $NewProgramFiles" -ForegroundColor "Cyan"
		New-Item -Path ${env:ProgramFiles(x86)}\ -Name "Microsoft Visual Studio" -ItemType Directory -ErrorAction SilentlyContinue
		New-Item -ItemType Directory -ErrorAction SilentlyContinue "$NewProgramFiles\Microsoft Visual Studio\Shared"
		New-Item -ItemType Directory -ErrorAction SilentlyContinue "$NewProgramFiles\Microsoft SDKs"
		New-Item -ItemType Directory -ErrorAction SilentlyContinue "$NewProgramFiles\Windows Kits"
		New-Item -ItemType Directory -ErrorAction SilentlyContinue "$NewProgramFiles\dotnet"
		New-Item -ItemType SymbolicLink -Path "${env:ProgramFiles(x86)}\Microsoft Visual Studio\Shared" -Target "$NewProgramFiles\Microsoft Visual Studio\Shared"
		New-Item -ItemType SymbolicLink -Path "${env:ProgramFiles(x86)}\Microsoft SDKs" -Target "$NewProgramFiles\Microsoft SDKs"
		New-Item -ItemType SymbolicLink -Path "${env:ProgramFiles(x86)}\Windows Kits" -Target "$NewProgramFiles\Windows Kits"
		New-Item -ItemType SymbolicLink -Path "${env:ProgramFiles(x86)}\dotnet" -Target "$NewProgramFiles\dotnet"
	}
	# Packages
	if (Get-Command winget -ErrorAction SilentlyContinue)
	{
		Write-Host "Installing Visual Studio Community." -ForegroundColor "Cyan"
		winget install --exact --id Microsoft.VisualStudio.2022.Community --accept-source-agreements --accept-package-agreements --override $VS_Params
		winget install --accept-source-agreements --accept-package-agreements Microsoft.DotNet.Framework.DeveloperPack.4.5 OpenVPNTechnologies.OpenVPN dnSpyEx.dnSpy UPX.UPX python3 WiXToolset.WiXToolset WerWolv.ImHex Microsoft.Office

	}
	elseif (Get-Command choco -ErrorAction SilentlyContinue)
	{
		choco install -y visualstudio2022community --package-parameters $VS_Params
		choco install -y netfx-4.5.2-devpack microsoft-office-deployment openvpn dnspyex upx python wixtoolset imhex
	}
	else
	{
		Write-Host "Could not install Visual Studio. Winget and Chocolatey are not available." -ForegroundColor "Red"
	}
}

$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine"), [System.Environment]::GetEnvironmentVariable("Path","User") -Join [IO.Path]::PathSeparator
python -m pip install --upgrade pip
pip3 install frida-tools

if (Get-Command choco -ErrorAction SilentlyContinue)
{
	choco install -y sysinternals --params "/InstallDir:$env:TOOLS\sysinternals"
}

if (-Not(Get-Module -ListAvailable -Name GPRegistryPolicy))
{
	Install-Module GPRegistryPolicy -Force
}

if ((Get-ComputerInfo -Property WindowsInstallationType).WindowsInstallationType -eq "Server")
{
	Install-WindowsFeature RSAT-AD-PowerShell
}
elseif (-Not($IsContainer))
{
	Get-WindowsCapability -Name Rsat.ActiveDirectory* -Online | Where-Object {$_.State -ne 'Installed'} | Add-WindowsCapability -Online
	Get-WindowsCapability -Name Rsat.Dns* -Online | Where-Object {$_.State -ne 'Installed'} | Add-WindowsCapability -Online
}

Update-Help -Force -ErrorAction SilentlyContinue -ErrorVariable UpdateErrors

if (Test-Path "$PSScriptRoot\infosec_profile.ps1" -PathType leaf)
{
	Copy-Item "$PSScriptRoot\infosec_profile.ps1" -Destination "$HOME\.profile_custom.ps1"
}
else
{
	Write-Host "Did not find the $PSScriptRoot\infosec_profile.ps1 file." -ForegroundColor "Red"
}